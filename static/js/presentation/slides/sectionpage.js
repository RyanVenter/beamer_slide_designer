// /*
// * =================================================================================================
// *      Table of contents (toc)
// * =================================================================================================
// */
// sectionpagenum = addSlide();
//
// // var setTocInnerVerticalLayout = function () {
// //     var entry           = innertheme['toc'];
// //     var factor          = scalefactor;
// //
// //     var gap             = factor*10;
// //     var secheight       = factor*section_in_toc['height'];
// //     var subsecheight    = factor*subsection_in_toc['height'];
// //
// //     var boxheights      = 3*secheight + 6*subsecheight;
// //     var gaps            = 2*gap;
// //
// //     var topmargin   = (textheight() - (boxheights + gaps))/2;
// //
// //     var currtop = topmargin;
// //     for (var i in sectionsintoc) {
// //         console.log('setTocInnerVerticalLayout', sectionsintoc[i]['name']);
// //         sectionsintoc[i].set({
// //             top:    currtop
// //         });
// //         currtop += secheight;
// //         for (var j in subsectionsintoc[i]) {
// //             subsectionsintoc[i][j].set({
// //                 top:    currtop
// //             });
// //             currtop += subsecheight;
// //         }
// //         currtop += gap;
// //     }
// //     slides[tocnum].renderAll();
// // };
// //
// //
// // var setTocItem = function (template, option) {
// //         // var template    = element + ' in toc';
// //         var name        = template + ' item';
// //
// //         var texttop     = innerelements[template + ' text']['top'];
// //         var fontsize    = getFontSize(template);
// //         var itemsize    = fontsize/3;
// //         var vsep        = (fontsize-itemsize)/2;
// //
// //
// //         var item;
// //         switch(option) {
// //             case 'default':
// //                 item = new fabric.Rect({
// //                     width:      0,
// //                     height:     0
// //                 });
// //                 break;
// //             case 'numbered':
// //                 item = new fabric.Text('X.X', {
// //                     top:        texttop,
// //                     fontSize:   fontsize
// //                 });
// //                 break;
// //             case 'circle':
// //                 item = new fabric.Circle({
// //                     radius:     itemsize/2
// //                 });
// //                 break;
// //             case 'square':
// //                 item = new fabric.Rect({
// //                     width:      itemsize,
// //                     height:     itemsize
// //                 });
// //                 break;
// //             // case 'ball':
// //             //     item = new fabric.Circle();
// //             //     break;
// //             // case 'ball unnumbered':
// //             //     break;
// //             // case 'image': //coming soon
// //             //     break;
// //             default:
// //                 console.log('TOC ITEM OPTION ERROR - ', option);
// //                 break;
// //         }
// //
// //         item.set({
// //             'name':     name,
// //             originY:    'center',
// //             top:        texttop + itemsize + vsep,
// //             fill:       getColor(template, 'fg', colortheme[template]['fg']),
// //         });
// //
// //         innerelements[name] = item;
// //         return item;
// // };
// //
// // var setTocTextAlignment = function (name) {
// //     var entry       = innertheme['toc'][name];
// //     var alignment   = entry['alignment'];
// //     var hspace      = mmToPx(entry['hspace*']);
// //     var sep;
// //     if ((opt = entry['hsep']) == 'empty') {
// //         sep = 5;
// //     } else {
// //         sep = emToPx(entry[opt]);               //NOTE EM-TO-PX NOT PT
// //     }
// //
// //     var text = innerelements[name + ' text'];
// //     var item = innerelements[name + ' item'];
// //     var val = 0;
// //     var itemsep = 0;
// //     switch (alignment) {
// //         case 'left':
// //             val = leftspace() + sep + hspace;
// //             console.log('left - val = ',val);
// //
// //             break;
// //         case 'center':
// //             val = leftspace() + (textwidth()/2) + hspace;
// //             console.log('center - val = ',val);
// //             break;
// //         case 'right':
// //             val = paperwidth() - rightspace() - sep;
// //             console.log('right - val = ',val);
// //
// //             break;
// //         default:
// //             console.log("ALIGNMENT PROPERTY OF", template, ' - ', name, ' NOT SET');
// //             break;
// //     }
// //
// //     item.set({
// //         originX:    alignment,
// //         left:       val
// //     });
// //
// //     if (item['type'] != 'text') {
// //         itemsep = 2;
// //     }
// //     text.set({
// //         originX:    alignment,
// //         left:       val + item['width'] + itemsep
// //     });
// // };
// //
// //
// // /*
// // * ------------------------------------------------------------------------------------------------
// // *      Section in toc
// // * ------------------------------------------------------------------------------------------------
// // */
// //
// // var sectionsintoc;  //Array of section object groups
// //
// // //---- Section in toc functions ------------------------------------------------------------------
// //
// // var refreshSectionsintoc = function () {
// //     sectionsintoc = [innertemplates['section in toc'], innertemplates['section in toc 2'], innertemplates['section in toc 3']];
// // };
// //
// // var removeSectionsInToc = function () {
// //     for (var i in sectionsintoc) {
// //         slides[tocnum].remove(sectionsintoc[i]);
// //     }
// // };
// // var setSectionInTocItem = function (option) {
// //     removeSectionsInToc();
// //
// //     section_in_toc_item = setTocItem('section in toc', option);
// //     setTocTextAlignment('section in toc');
// //     section_in_toc_box.set({
// //         left:   leftspace()
// //     });
// //     section_in_toc  = immutableGroup('section in toc', [section_in_toc_item, section_in_toc_text, section_in_toc_box]);
// //
// //     cloneAndAddSectionsInToc(option);
// //     setTocInnerVerticalLayout();
// //
// // };
// //
// // var cloneAndAddSectionsInToc = function(option ='default') {
// //     for (var i = 2; i < 4; i++) {
// //         var temp = immutableGroup('section in toc ' + i, [fabric.util.object.clone(section_in_toc_item), section_in_toc_text, section_in_toc_box]);
// //         temp.set({
// //             originX:    'left',
// //             left:       leftspace()
// //         });
// //         addInnerTemplate(temp, tocnum);
// //     }
// //
// //     addInnerTemplate(section_in_toc, tocnum);
// //
// //     refreshSectionsintoc();
// //
// //     if (option == 'numbered') {
// //         for (var i in sectionsintoc) {
// //             sectionsintoc[i].item(0).setText((parseInt(i)+1)+ '.');
// //         }
// //     }
// // };
// //
// //---- Section in toc variables ------------------------------------------------------------------
//
// // Section in toc box
// var section_title_box = innerBox('section title box');
// section_title_box.set({
//     fill:   'rgba(0,0,0,0)' //getColor('section in toc', 'bg', colortheme['section in toc']['bg']),
// });
//
// // Section in toc text
// var section_title_text = innerText('section title text', innertheme['section page']['section title']['text']);
// section_title_text.set({
//     fill:       getColor('section title', 'fg', colortheme['section title']['fg']),
//     fontSize:	getFontSize('section title'),
// });
//
// setInnerBoxHeight('section page', 'section title');
// setInnerTextTop('section page', 'section title');
//
// // Section in section page item
// var section_title_item = setTocItem('section title', 'default');
// setTocTextAlignment('section title');
// //
// // // Section in section page
// var section_title = immutableGroup('section title', [section_title_item, section_in_toc_text, section_in_toc_box]);
