








/*
* =================================================================================================
*      Part page
* =================================================================================================
*/

/*
* =================================================================================================
*      Section page
* =================================================================================================
*/

/*
* =================================================================================================
*      Subsection page
* =================================================================================================
*/



/*
* =================================================================================================
*      (sub)-section in toc [Section and subsections in toc]
* =================================================================================================
*/


/*
* =================================================================================================
*      (sub)-section in toc shaded [Section and subsections in toc shaded]
* =================================================================================================
*/

/*
* =================================================================================================
*      Item
* =================================================================================================
*/

/*
* =================================================================================================
*      Itemized items
* =================================================================================================
*/

/*
* =================================================================================================
*      Enumerate items
* =================================================================================================
*/

/*
* =================================================================================================
*      Description item width
* =================================================================================================
*/

/*
* =================================================================================================
*      Itemize/Enumerate body
* =================================================================================================
*/

/*
* =================================================================================================
*      Alerted text
* =================================================================================================
*/

// /*
// * =================================================================================================
// *      Structured text
// * =================================================================================================
// */

/*
* =================================================================================================
*      Bibliography items
* =================================================================================================
*/


/*
* =================================================================================================
*      Buttons
* =================================================================================================
*/

/*
* =================================================================================================
*      Abstract
* =================================================================================================
*/

/*
* =================================================================================================
*      Verse
* =================================================================================================
*/

/*
* =================================================================================================
*      Quotation
* =================================================================================================
*/

/*
* =================================================================================================
*      Quote
* =================================================================================================
*/

/*
* =================================================================================================
*      Footnotes
* =================================================================================================
*/

/*
* =================================================================================================
*      Captions
* =================================================================================================
*/

/*
* =================================================================================================
*      Blocks
* =================================================================================================
*/

/*
* =================================================================================================
*      Theorems
* =================================================================================================
*/


/*
* =================================================================================================
*       Proofs
* =================================================================================================
*/



// var shadeDown = {
//    color: 'rgba(0,0,0,0.6)',
//    blur: 10,
//    offsetX: 5,
//    offsetY: 5,
//    opacity: 0.6,
//    fillShadow: true,
//    strokeShadow: true
// }
// /* =================================================================================
// * === CANVAS FUNCTIONS ============================================================
// * =============================================================================== */
// var active_elem= "";
//
// var setActiveElem = function(elem) {
//     active_elem = elem.name;
// }
//
// background_canvas.on('mousedown', function() {
//    console.log('Element selection:', background_canvas.fg_dependency);
//    var name = background_canvas.name;
//    if (active_elem != name) {
//        setActiveElem = name;
//    }
// });
//
// var setBlockColorToolValues = function(elem) {
//    if (active_elem != elem.name) {
//        canvas.setActiveObject(elem);
//        canvas2.setActiveObject(elem);
//        canvas3.setActiveObject(elem);
//
//        for (var i in slides) {
//            slides[i].setActiveObject(elem);
//        }
//
//        $.ajax({
//            type: 'GET',
//            url: $FILE_ROOT+"html/toolbox/block_color_tools.html",
//            success: function (data) {
//                console.log('Success', $FILE_ROOT+"html/toolbox/block_color_tools.html has been loaded");
//                $('#color_tools').html('');
//                $('#color_tools').append($(data));
//                $('#color_tools').append($.getScript($FILE_ROOT+"js/toolbox/block_color_tools.js", function() {
//                    for (var i = 0; i < block_jscolorpickers.length; i++) {
//                        document.getElementById(block_jscolorpickers[i]).style.backgroundColor=elem.item(i).fill;
//                    }
//                }));
//            }
//        });
//
//        $.ajax({
//            type: 'GET',
//            url: $FILE_ROOT+"html/toolbox/block_font_tools.html",
//            success: function (data) {
//                console.log('Success', data);
//                $('#font_tools').html('');
//                $('#font_tools').append($(data));
//            }
//        });
//
//        active_elem = elem.name;
//
//        canvas.renderAll();
//        canvas2.renderAll();
//        canvas3.renderAll();
//
//        for (var i in slides) {
//            slides[i].renderAll();
//        }
//
//    }
// };
//
// block_group.on('mousedown', function() {
//    setBlockColorToolValues(block_group);
// });
//
// block_alerted_group.on('mousedown', function() {
//    setBlockColorToolValues(block_alerted_group);
// });
//
// block_example_group.on('mousedown', function() {
//    setBlockColorToolValues(block_example_group);
// });
