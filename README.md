# Beamer Slide Designer #
 The Beamer Slide Designer (BSD) is a web-based application developed for the graphical construction of custom Beamer themes. The constructed themes may be exported as theme packages containing TeX/LaTeX compatible style files, intended to be used in Beamer presentation documents.

 ![theme_builder_interface.png](https://bitbucket.org/repo/pLqXoG/images/2864230882-theme_builder_interface.png)

 BSD has been created in partial fulfillment of a BSc Computer Science Honours Project at Stellenbosch University.

 The application is hosted at [http://ryanventer.pythonanywhere.com/](Link URL) and will be live until **17 April 2017**
