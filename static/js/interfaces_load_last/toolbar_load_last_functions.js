/*
 * ================================================================================================
 *     Colors
 * ================================================================================================
*/
var refreshToolbarColorFunctions = function() {
    //structure
    var structure_color = getColor('structure', 'fg', colortheme['structure']['fg']);
    $('.structure-color').css('color', structure_color);
    document.getElementById('structure_color_fg').style.backgroundColor=structure_color;

    //palette
    var palette = ['primary', 'secondary', 'tertiary', 'quaternary'];
    for (var i in palette) {
        var palette_color = getColor('palette ' + palette[i], 'fg', colortheme['palette ' + palette[i]]['fg'])
        $('.palette-' + palette[i] + '-color').css('color', palette_color);
        document.getElementById('palette_' + palette[i] + '_color_fg').style.backgroundColor=palette_color;

        var palette_sidebar_color = getColor('palette sidebar ' + palette[i], 'fg', colortheme['palette sidebar ' + palette[i]]['fg'])
        $('.palette-sidebar-' + palette[i] + '-color').css('color', palette_sidebar_color);
        document.getElementById('palette_sidebar_' + palette[i] + '_color_fg').style.backgroundColor=palette_sidebar_color;
    }
};

$(document).ready(refreshToolbarColorFunctions());

var getRGB = function(elem) {
    var red         = Math.round(elem.rgb[0]);
    var green       = Math.round(elem.rgb[1]);
    var blue        = Math.round(elem.rgb[2]);

    var value       = 'rgb(' + red + ',' + green + ',' + blue + ')';
    return value;
};

var updateStructureFg = function(elem) {

    var value = getRGB(elem);

    colortheme['structure']['fg'] = {
        'type':     'color',
        'value':    value
    }

    refreshSlideColors();
};

var updatePaletteFg = function(target, value) {
    colortheme[target]['fg'] = {
        'type':     'color',
        'value':    value
    }

    refreshSlideColors();
};

var updatePalettePrimaryFg = function(elem) {
    var value = getRGB(elem);
    updatePaletteFg('palette primary', value);
    // $('.palette-primary-color').css('color', value);

};

var updatePaletteSecondaryFg = function(elem) {
    var value = getRGB(elem);
    updatePaletteFg('palette secondary', value);
    // $('#palette_secondary_text').css('color', value);
};
var updatePaletteTertiaryFg = function(elem) {
    var value = getRGB(elem);
    updatePaletteFg('palette tertiary', value);
    // $('#palette_tertiary_text').css('color', value);
};
var updatePaletteQuaternaryFg = function(elem) {
    var value = getRGB(elem);
    updatePaletteFg('palette quaternary', value);
    // $('#palette_quaternary_text').css('color', value);
};

var updatePaletteSidebarFg = function(target, value) {
    colortheme[target]['fg'] = {
        'type':     'color',
        'value':    value
    }
    refreshSlideColors();
};

var updatePaletteSidebarPrimaryFg = function(elem) {
    var value = getRGB(elem);
    updatePaletteSidebarFg('palette sidebar primary', value);
    // $('.palette sidebar-primary-color').css('color', value);

};

var updatePaletteSidebarSecondaryFg = function(elem) {
    var value = getRGB(elem);
    updatePaletteSidebarFg('palette sidebar secondary', value);
    // $('#palette sidebar_secondary_text').css('color', value);
};
var updatePaletteSidebarTertiaryFg = function(elem) {
    var value = getRGB(elem);
    updatePaletteSidebarFg('palette sidebar tertiary', value);
    // $('#palette sidebar_tertiary_text').css('color', value);
};
var updatePaletteSidebarQuaternaryFg = function(elem) {
    var value = getRGB(elem);
    updatePaletteSidebarFg('palette sidebar quaternary', value);
    // $('#palette sidebar_quaternary_text').css('color', value);
};
