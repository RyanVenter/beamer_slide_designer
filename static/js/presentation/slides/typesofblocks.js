/*
* =================================================================================================
*      Types of blocks
* =================================================================================================
*/
typesofblocksnum = addSlide('types of blocks');
slides[typesofblocksnum]['verticalLayoutFunction'] = function () { setTypesOfBlocksInnerVerticalLayout(); };


var setTypesOfBlocksInnerVerticalLayout = function () {
    var entry               = innertheme['types of blocks'];
    var blocktitle          = innertemplates['block title'];
    var blockbody           = innertemplates['block body'];
    var blocktitlealerted   = innertemplates['block title alerted'];
    var blockbodyalerted    = innertemplates['block body alerted'];
    var blocktitleexample   = innertemplates['block title example'];
    var blockbodyexample    = innertemplates['block body example'];

    var blocktitleheight        = scalefactor*blocktitle['height'];
    var blockbodyheight         = scalefactor*blockbody['height'];
    var blocktitlealertedheight = scalefactor*blocktitlealerted['height'];
    var blockbodyalertedheight  = scalefactor*blockbodyalerted['height'];
    var blocktitleexampleheight = scalefactor*blocktitleexample['height'];
    var blockbodyexampleheight  = scalefactor*blockbodyexample['height'];
    var gap                     = scalefactor*15;



    var blocktitlerounded   = innertheme['types of blocks']['block title']['rounded'];
    var blocktitleshadow    = innertheme['types of blocks']['block title']['shadow'];
    var blocktitlespace     = (blocktitlerounded == blocktitleshadow && blocktitleshadow == 'true' && colortheme['block title']['bg']['type'].indexOf('color') != -1);

    var blocktitlealertedrounded   = innertheme['types of blocks']['block title alerted']['rounded'];
    var blocktitlealertedshadow    = innertheme['types of blocks']['block title alerted']['shadow'];
    var blocktitlealertedspace     = (blocktitlealertedrounded == blocktitlealertedshadow && blocktitlealertedshadow == 'true' && colortheme['block title alerted']['bg']['type'].indexOf('color') != -1);


    var blocktitleexamplerounded   = innertheme['types of blocks']['block title example']['rounded'];
    var blocktitleexampleshadow    = innertheme['types of blocks']['block title example']['shadow'];
    var blocktitleexamplespace     = (blocktitleexamplerounded == blocktitleexampleshadow && blocktitleexampleshadow == 'true' && colortheme['block title example']['bg']['type'].indexOf('color') != -1);




    var shadow      = 5;
    var shadowcount = 0;
    if (blocktitlespace) {
        shadowcount += 1;
    }
    if (blocktitlealertedspace) {
        shadowcount += 1;
    }
    if (blocktitleexamplespace) {
        shadowcount += 1;
    }


    var boxheights  = blocktitleheight + blockbodyheight + blocktitlealertedheight + blockbodyalertedheight + blocktitleexampleheight + blockbodyexampleheight;
    var gaps        = 2*gap;
    var shadows     = shadowcount*shadow;

    var topmargin   = (scalefactor*textheight() - (boxheights+gaps+shadows))/2;
    var currtop     = topmargin;

    blocktitle.set({
        top:    currtop
    });
    currtop += blocktitleheight;
    if (blocktitlespace) {
        currtop += shadow;
    }
    blockbody.set({
        top:    currtop
    });
    currtop += blockbodyheight+gap;
    blocktitlealerted.set({
        top:    currtop
    });
    currtop += blocktitlealertedheight;
    if (blocktitlealertedspace) {
        currtop += shadow;
    }
    blockbodyalerted.set({
        top:    currtop
    });
    currtop += blockbodyalertedheight+gap;
    blocktitleexample.set({
        top:    currtop
    });
    currtop += blocktitleexampleheight;
    if (blocktitleexamplespace) {
        currtop += shadow;
    }
    blockbodyexample.set({
        top:    currtop
    });

    slides[typesofblocksnum].renderAll();
};


/* ---- Refresh slide fonts --------------------------------------------------------------------- */
var refreshTypesOfBlocksFonts = function (name) {
    updateTemplateFont(innertemplates['block title'],'types of blocks', typesofblocksnum);
    updateTemplateFont(innertemplates['block body'],'types of blocks', typesofblocksnum);

    updateTemplateFont(innertemplates['block title alerted'],'types of blocks', typesofblocksnum);
    updateTemplateFont(innertemplates['block body alerted'],'types of blocks', typesofblocksnum);

    updateTemplateFont(innertemplates['block title example'],'types of blocks', typesofblocksnum);
    updateTemplateFont(innertemplates['block body example'],'types of blocks', typesofblocksnum);

    setTypesOfBlocksInnerVerticalLayout();
    // slides[typesofblocksnum].renderAll();
    if (name != 'empty') {
        slides[typesofblocksnum].setActiveObject(innertemplates[name]);
    }
    slides[typesofblocksnum].renderAll();
};


/* ---- Refresh slide colors -------------------------------------------------------------------- */
var refreshTypesOfBlocksColors = function () {
    var templates = ['block title', 'block body'];
    var extensions = ['', ' alerted', ' example'];
    for (var i in templates) {
        for (var j in extensions) {
            var name = templates[i] + extensions[j];
            innerelements[name + ' text'].set({
                fill:   getColor(name, 'fg', colortheme[name]['fg'])
            });

            innerelements[name + ' box'].set({
                fill:   getColor(name, 'bg', colortheme[name]['bg'])
            });
        }
    }

    // slides[typesofblocksnum].renderAll();
    refreshTypesOfBlocksShadows();
};

/* ---- Refresh slide shadows ------------------------------------------------------------------- */
var refreshTypesOfBlocksShadows = function () {
    var bases        = ['block title', 'block body'];
    var templates   = ['', ' alerted', ' example'];
    var shadow      = innertheme['types of blocks']['block title']['shadow'];

    for (var i in bases) {
        for (var j in templates) {
            var target  = bases[i] + templates[j];
            var rounded = innertheme['types of blocks'][target]['rounded'];
            var shadow = innertheme['types of blocks'][target]['shadow'];
            setTemplateShadow(target, rounded, shadow);
        }
        //
        // var target = 'block title' + templates[i];
        // var body = 'block body' + templates[i];
        //
        // var rounded     = innertheme['types of blocks'][title]['rounded'];
        // var shaodw     = innertheme['types of blocks']['block title']['shadow'];
        //
        //
        //
        // setTemplateShadow('block title' + name, innertheme['types of blocks']['block title']['rounded'], innertheme['types of blocks']['block title']['shadow']);
        // setTemplateShadow('block body' + name, rounded, shadow);
    }
    setTypesOfBlocksInnerVerticalLayout();
    slides[typesofblocksnum].renderAll();
};


/* ---- Attach update function to slide -------------------------------------------------------- */
slides[typesofblocksnum].refreshFontsFunction = function (name) { refreshTypesOfBlocksFonts(name); };
slides[typesofblocksnum].refreshColorsFunction = function () { refreshTypesOfBlocksColors(); };
slides[typesofblocksnum].refreshShadowsFunction = function () { refreshTypesOfBlocksShadows(); };


/*
* ------------------------------------------------------------------------------------------------
*      Block title
* ------------------------------------------------------------------------------------------------
*/
//---- Block title variables ---------------------------------------------------------------------

// Block title box
var block_title_box = innerBox('block title box');
block_title_box.set({
    fill:   getColor('block title', 'bg', colortheme['block title']['bg']),
});

// Block title text
var block_title_text = innerText('block title text', innertheme['types of blocks']['block title']['text']);
block_title_text.set({
    fill:       getColor('block title', 'fg', colortheme['block title']['fg']),
    fontSize:	getFontSize('block title'),
    fontFamily: getFontFamily('block title')
});
// wrapText(block_title_text, typesofblocksnum, textwidth());
setInnerBoxHeight('types of blocks', 'block title');
setInnerTextTop('types of blocks', 'block title');
initialiseInnerTextAlignment('types of blocks', 'block title');

// Block title
var block_title = immutableGroup('block title', [block_title_text, block_title_box], typesofblocksnum);
addInnerTemplate(block_title, typesofblocksnum);

/*
* ------------------------------------------------------------------------------------------------
*      Block body
* ------------------------------------------------------------------------------------------------
*/
//---- Block body variables ---------------------------------------------------------------------

// Block body box
var block_body_box = innerBox('block body box');
block_body_box.set({
    fill:   getColor('block body', 'bg', colortheme['block body']['bg']),
});

// Block body text
var block_body_text = innerText('block body text', innertheme['types of blocks']['block body']['text']);
block_body_text.set({
    fill:       getColor('block body', 'fg', colortheme['block body']['fg']),
    fontSize:	getFontSize('block body'),
    fontFamily: getFontFamily('block body'),

});
wrapText(block_body_text, typesofblocksnum, textwidth());
setInnerBoxHeight('types of blocks', 'block body');
setInnerTextTop('types of blocks', 'block body');
initialiseInnerTextAlignment('types of blocks', 'block body');

// Block body
var block_body = immutableGroup('block body', [block_body_text, block_body_box], typesofblocksnum);
addInnerTemplate(block_body, typesofblocksnum);

/*
* ------------------------------------------------------------------------------------------------
*      Block title alerted
* ------------------------------------------------------------------------------------------------
*/
//---- Block title alerted variables ---------------------------------------------------------------------

// Block title alerted box
var block_title_alerted_box = innerBox('block title alerted box');
block_title_alerted_box.set({
    fill:   getColor('block title alerted', 'bg', colortheme['block title alerted']['bg']),
});

// Block title alerted text
var block_title_alerted_text = innerText('block title alerted text', innertheme['types of blocks']['block title alerted']['text']);
block_title_alerted_text.set({
    fill:       getColor('block title alerted', 'fg', colortheme['block title alerted']['fg']),
    fontSize:	getFontSize('block title alerted'),
    fontFamily: getFontFamily('block title alerted')

});
// wrapText(block_title_alerted_text, typesofblocksnum, textwidth());
setInnerBoxHeight('types of blocks', 'block title alerted');
setInnerTextTop('types of blocks', 'block title alerted');
initialiseInnerTextAlignment('types of blocks', 'block title alerted');

// Block title alerted
var block_title_alerted = immutableGroup('block title alerted', [block_title_alerted_text, block_title_alerted_box], typesofblocksnum);
addInnerTemplate(block_title_alerted, typesofblocksnum);

/*
* ------------------------------------------------------------------------------------------------
*      Block body alerted
* ------------------------------------------------------------------------------------------------
*/
//---- Block body alerted variables ---------------------------------------------------------------------

// Block body alerted box
var block_body_alerted_box = innerBox('block body alerted box');
block_body_alerted_box.set({
    fill:   getColor('block body alerted', 'bg', colortheme['block body alerted']['bg']),
});

// Block body alerted text
var block_body_alerted_text = innerText('block body alerted text', innertheme['types of blocks']['block body alerted']['text']);
block_body_alerted_text.set({
    fill:       getColor('block body alerted', 'fg', colortheme['block body alerted']['fg']),
    fontSize:	getFontSize('block body alerted'),
    fontFamily: getFontFamily('block body alerted')

});
wrapText(block_body_alerted_text, typesofblocksnum, textwidth());
setInnerBoxHeight('types of blocks', 'block body alerted');
setInnerTextTop('types of blocks', 'block body alerted');
initialiseInnerTextAlignment('types of blocks', 'block body alerted');

// Block body alerted
var block_body_alerted = immutableGroup('block body alerted', [block_body_alerted_text, block_body_alerted_box], typesofblocksnum);
addInnerTemplate(block_body_alerted, typesofblocksnum);

/*
* ------------------------------------------------------------------------------------------------
*      Block title example
* ------------------------------------------------------------------------------------------------
*/
//---- Block title example variables ---------------------------------------------------------------------

// Block title example box
var block_title_example_box = innerBox('block title example box');
block_title_example_box.set({
    fill:   getColor('block title example', 'bg', colortheme['block title example']['bg']),
});

// Block title example text
var block_title_example_text = innerText('block title example text', innertheme['types of blocks']['block title example']['text']);
block_title_example_text.set({
    fill:       getColor('block title example', 'fg', colortheme['block title example']['fg']),
    fontSize:	getFontSize('block title example'),
    fontFamily: getFontFamily('block title example')

});
// wrapText(block_title_example_text, typesofblocksnum, textwidth());
setInnerBoxHeight('types of blocks', 'block title example');
setInnerTextTop('types of blocks', 'block title example');
initialiseInnerTextAlignment('types of blocks', 'block title example');

// Block title example
var block_title_example = immutableGroup('block title example', [block_title_example_text, block_title_example_box], typesofblocksnum);
addInnerTemplate(block_title_example, typesofblocksnum);

/*
* ------------------------------------------------------------------------------------------------
*      Block body example
* ------------------------------------------------------------------------------------------------
*/
//---- Block body example variables ---------------------------------------------------------------------

// Block body example box
var block_body_example_box = innerBox('block body example box');
block_body_example_box.set({
    fill:   getColor('block body example', 'bg', colortheme['block body example']['bg']),
});

// Block body example text
var block_body_example_text = innerText('block body example text', innertheme['types of blocks']['block body example']['text']);
block_body_example_text.set({
    fill:       getColor('block body example', 'fg', colortheme['block body example']['fg']),
    fontSize:	getFontSize('block body example'),
    fontFamily: getFontFamily('block body example')

});
wrapText(block_body_example_text, typesofblocksnum, textwidth());
setInnerBoxHeight('types of blocks', 'block body example');
setInnerTextTop('types of blocks', 'block body example');
initialiseInnerTextAlignment('types of blocks', 'block body example');

// Block body example
var block_body_example = immutableGroup('block body example', [block_body_example_text, block_body_example_box], typesofblocksnum);
addInnerTemplate(block_body_example, typesofblocksnum);

setTypesOfBlocksInnerVerticalLayout();
