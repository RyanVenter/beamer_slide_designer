

var getFontSize = function (name='normal text') {
    var fontsize = ptToPx(config['basefontsize']);
    if ((size = fonttheme[name]['size']) != 'empty') {
        return ptToPx(fontsizes[config['basefontsize']][size]);
    } else if ((parent = fonttheme[name]['parent']) != 'empty') {
        if (parent.indexOf(',') == -1) {
            return getFontSize(parent);
        } else {
            var start   = 1;
            var end     = parent.length -1;
            parents     = parent.substring(start,end).split(',');
            // Check is parent size is explicitly defined (in precendence order)
            for (var i = parents.length - 1; i >=0 ; i--) {
                if (fonttheme[parents[i]]['size'] != 'empty') {
                    return getFontSize(parents[i]);
                }
            }
            // Check is parent inherited size is not normalsize (effectively 'empty')
            for (var i = parents.length - 1; i >=0 ; i--) {
                var inheritedsize = getFontSize(parents[i]);
                if (inheritedsize != fontsize) {
                    return inheritedsize;
                }
            }
            // Return basefontsize
            return fontsize;
        }
    } else {
        return fontsize;
    }
};


var getFontFamilyOnly = function (name) {
    var familydefault   = fonttheme['familydefault'];
    var fontfamily      = fonttheme[familydefault]['entry'];
    if ((family = fonttheme[name]['family']) != 'empty') {
        return fonttheme[family]['entry']
    } else if ((parent = fonttheme[name]['parent']) != 'empty') {
        if (parent.indexOf(',') == -1) {
            return getFontFamilyOnly(parent);
        } else {
            var start   = 1;
            var end     = parent.length -1;
            var parents     = parent.substring(start,end).split(',');

            // Check is parent family is explicitly defined (in precendence order)
            for (var i = parents.length - 1; i >=0 ; i--) {
                if (fonttheme[parents[i]]['family'] != 'empty') {
                    return getFontFamilyOnly(parents[i]);
                }
            }
            // Check is parent inherited family is not default (effectively 'empty')
            for (var i = parents.length - 1; i >=0 ; i--) {
                var inheritedfamily = getFontFamilyOnly(parents[i]);
                if (inheritedfamily != fontfamily) {
                    return inheritedfamily;
                }
            }
            // Return default fontfamily
            return fontfamily;
        }
    }
    return fontfamily;
};

var getFontSeriesOnly = function (name) {
    var fontseries = ''
    if ((series = fonttheme[name]['series']) != 'empty') {
        if (series != 'mdseries') {
            return ('-' + series);
        }
    } else if ((parent = fonttheme[name]['parent']) != 'empty') {
        if (parent.indexOf(',') == -1) {
            return getFontSeriesOnly(parent);
        } else {
            var start   = 1;
            var end     = parent.length -1;
            var parents     = parent.substring(start,end).split(',');

            // Check is parent series is explicitly defined (in precendence order)
            for (var i = parents.length - 1; i >=0 ; i--) {
                if (fonttheme[parents[i]]['series'] != 'empty') {
                    return getFontSeriesOnly(parents[i]);
                }
            }
            // Check is parent inherited series is not default (effectively 'empty')
            for (var i = parents.length - 1; i >=0 ; i--) {
                var inheritedseries = getFontSeriesOnly(parents[i]);
                if (inheritedseries != fontseries) {
                    return inheritedseries;
                }
            }
            // Return  default fontseries
            return fontseries;
        }
    }
    return fontseries;
};

var getFontShapeOnly = function (name) {
    var fontshape = ''
    if ((shape = fonttheme[name]['shape']) != 'empty') {
        return ('-' + shape);
    } else if ((parent = fonttheme[name]['parent']) != 'empty') {
        if (parent.indexOf(',') == -1) {
            return getFontShapeOnly(parent);
        } else {
            var start   = 1;
            var end     = parent.length -1;
            var parents     = parent.substring(start,end).split(',');

            // Check is parent shape is explicitly defined (in precendence order)
            for (var i = parents.length - 1; i >=0 ; i--) {
                if (fonttheme[parents[i]]['shape'] != 'empty') {
                    return getFontShapeOnly(parents[i]);
                }
            }
            // Check is parent inherited shape is not default (effectively 'empty')
            for (var i = parents.length - 1; i >=0 ; i--) {
                var inheritedshape = getFontShapeOnly(parents[i]);
                if (inheritedshape != fontshape) {
                    return inheritedshape;
                }
            }
            // Return  default fontshape
            return fontshape;

        }
    }
    return fontshape;
};
var getFontFamily = function (name='normal text') {
    var fontfamily = getFontFamilyOnly(name);
    fontfamily += getFontSeriesOnly(name);
    fontfamily += getFontShapeOnly(name);

    return fontfamily;
};
