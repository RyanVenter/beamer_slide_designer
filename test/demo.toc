\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Section: Types of text}{3}{0}{1}
\beamer@sectionintoc {2}{Section: Types of blocks}{6}{0}{2}
\beamer@sectionintoc {3}{Section: Lists}{7}{0}{3}
\beamer@subsectionintoc {3}{1}{Subsection: Itemized lists}{7}{0}{3}
\beamer@subsectionintoc {3}{2}{Subsection: Enumerated lists}{8}{0}{3}
