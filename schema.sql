drop table if exists elements;
create table elements (
	name text primary key,
	left integer,
	top integer,
	width integer,
	height integer,
	fg text,
	bg text,
	'text' text,
	fontSize integer,
	slide text
);
