/*
 * ================================================================================================
 *     Settings
 * ================================================================================================
*/

//basefontsize
$('.basefontsize-option').on('mousedown', function() {
    config['basefontsize'] = $(this).attr('data-value');
    refreshSlides();
});

//aspectratio
$('.aspectratio-option').on('mousedown', function() {
    config['aspectratio'] = $(this).attr('data-value');
    // scaleAll();
    scaleToAspectRatio();
    refreshSlides();
});

// //text margin left
// $('#text_margin_left_selection').on('input', function() {
//     config['text margin left'] = $('#text_margin_left_selection').val();
//     refreshSlideFonts();
// });
//
// //text margin right
// $('#text_margin_right_selection').on('input', function() {
//     config['text margin right'] = $('#text_margin_right_selection').val();
//     refreshSlideFonts();
// });


/*
 * ================================================================================================
 *     Colors
 * ================================================================================================
*/

/* These functions are located in ./static/js/interfaces_load_last/toolbar_load_last_functions.js*/

/*
 * ================================================================================================
 *     Fonts
 * ================================================================================================
*/
var fontItemScript = function(family, index, fontname, fontface) {
    return  "<div class=\"item fontfamily-option\" data-family=\""+family+"\" data-index=\""+index+"\" >" +
                "<font face=\""+fontface+"\">"+fontname+"</font>" +
            "</div>";
};

var populateFontFamiliesMenu = function() {
    var families = ['sf','rm','tt'];
    for (var i in families) {
        var family = families[i];
        for (var index in fontfamilies[family]) {
            var fontname = fontfamilies[family][index]['name'];
            var fontface = fontfamilies[family][index]['entry'];
            $('#'+family+'family_menu').append(fontItemScript(family, index, fontname,fontface));
        }
    }
};
$(document).ready(populateFontFamiliesMenu());

$('.fontfamily-option').on('mousedown', function() {
    var family = $(this).attr('data-family');
    var index = $(this).attr('data-index');

    var fontfamily = fontfamilies[family][index];
    var target = fonttheme[family+'family'];
    target['name']     = fontfamily['name'];
    target['entry']    = fontfamily['entry'];
    target['package']  = fontfamily['package'];

    $('#'+family+'family_selection').css('font-family', fontfamily['entry']);
    $('#math_'+family+'family_selection').css('font-family', fontfamily['entry']);

    refreshSlides();
});

$('#familydefault_selection .item').on('mousedown', function() {
    fonttheme['familydefault'] = $(this).attr('data-value');
    refreshSlides();
});

$('#mathfamilydefault_selection .item').on('mousedown', function() {
    fonttheme['mathfamilydefault'] = $(this).attr('data-value');
});

/*
 * ================================================================================================
 *     Graphics
 * ================================================================================================
*/

// $('#titlepagecanvasgraphic_form').on('click', function(event) {
//     event.preventDefault();
//     console.log('hi');
//     $.ajax({
//            url : $SCRIPT_ROOT+"/upload",
//            type: 'POST',
//            data: $(this).serialize(),
//            success: function (data) {
//                alert('SUCCESS');
//            },
//            error: function (jXHR, textStatus, errorThrown) {
//                alert(errorThrown);
//            }
//        });
//    });

  $(document).ready(function () {
    $('#titlepagecanvasgraphic_form').on('mousedown', function(e) {
        e.preventDefault();
        // $.ajax({
        //     url : $(this).attr('action') || window.location.pathname,
        //     type: "GET",
        //     data: $(this).serialize(),
        //     success: function (data) {
        //         $("#form_output").html(data);
        //     },
        //     error: function (jXHR, textStatus, errorThrown) {
        //         alert(errorThrown);
        //     }
        // });
    });
});
