/*
* =================================================================================================
*      General template functions
* =================================================================================================
*/
var setActiveTemplate = function(name) {
    active_template = name;
}

var setActiveSlide = function (slidenum) {
    active_slide = slidenum;
}

var getActiveTemplate = function() {
    return active_template;
}

var getActiveSlide = function() {
    return active_slide;
}

var makeImmutable = function(target) {
    target.set({
        lockMovementX:	true,
        lockMovementY:	true,
        lockScalingX:	true,
        lockScalingY:	true,
        lockRotation:	true,
        hasRotatingPoint: false,
        cornerSize: 3
    });
};

var immutableGroup = function (name, members, slidenum=0, nameoverride='') {
    var group = new fabric.Group(members,{
        'name': name,
        'slidenum': slidenum,
        originY: 'top',
        lockMovementX:	true,
        lockMovementY:	true,
        lockScalingX:	true,
        lockScalingY:	true,
        lockRotation:	true,
        hasRotatingPoint: false,
        cornerSize: 3,
    });

    var slidename = slides[slidenum]['name'];


    group.on('mousedown', function() {
        //Set Color tool values
        if (name.indexOf('toc') != -1) {
            if (name.indexOf('sub') == -1) {
                name = 'section in toc'
            } else {
                name = 'subsection in toc'
            }
        }

        if (nameoverride != '') {
            setActiveTemplate(nameoverride);
        } else {
            setActiveTemplate(name);
        }
        setActiveSlide(slidenum);

        for (var i in slides) {
            if (i != slidenum) {
                slides[i].setActiveObject(innertemplates[name]);
            }
        }


        //Set Color tool values
        document.getElementById('toolbox_color_fg').style.backgroundColor=innerelements[name + ' text']['fill'];
        document.getElementById('toolbox_color_bg').style.backgroundColor=innerelements[name + ' box']['fill'];


        //Background color and alignment messages
        if (name == 'subtitle' || (toc = (name.indexOf('toc') != -1)) || name == 'alerted text') {
            $('#background_color_message').removeClass('hidden');
            $('#alignment_appearance_message').removeClass('hidden');

            $('#rounded_appearance_message1').addClass('hidden');
            $('#rounded_appearance_message2').removeClass('hidden');

            $('#shadow_appearance_message1').addClass('hidden');
            $('#shadow_appearance_message2').removeClass('hidden');


        } else {
            $('#background_color_message').addClass('hidden');
            $('#alignment_appearance_message').addClass('hidden');

            $('#rounded_appearance_message1').removeClass('hidden');
            $('#rounded_appearance_message2').addClass('hidden');

            $('#shadow_appearance_message1').removeClass('hidden');
            $('#shadow_appearance_message2').addClass('hidden');
        }

        // Set Font tool values
        // Set size dropdown value
        var size  = fonttheme[name]['size']
        if (size == 'empty') {
            size = 'default'
        }
        $('#font_size_selection').dropdown('set selected', size);

        // Set series dropdown value
        var series  = fonttheme[name]['series']
        if (series == 'empty') {
            series = 'default'
        }
        $('#font_series_selection').dropdown('set selected', series);

        // Set shape dropdown value
        var shape  = fonttheme[name]['shape']
        if (shape == 'empty') {
            shape = 'default'
        }
        $('#font_shape_selection').dropdown('set selected', shape);

        // Set family dropdown value
        var family  = fonttheme[name]['family']
        if (family == 'empty') {
            family = 'default'
        }
        $('#font_family_selection').dropdown('set selected', family);


        //Structure message
        if (name == 'normal text') {
            $('#structure_font_message').removeClass('hidden');
        } else {
            $('#structure_font_message').addClass('hidden');
        }

        //Set appearance tool values
        $('#alignment_selection').dropdown('set selected', innertheme[slidename][name]['alignment']);
        $('#rounded_selection').dropdown('set selected', innertheme[slidename][name]['rounded']);
        $('#shadow_selection').dropdown('set selected', innertheme[slidename][name]['shadow']);

        //Alignment message for normal text
        if (name == 'normal text') {
            $('#alignment_appearance_message').removeClass('hidden');

            $('#rounded_appearance_message1').addClass('hidden');
            $('#rounded_appearance_message2').removeClass('hidden');

            $('#shadow_appearance_message1').addClass('hidden');
            $('#shadow_appearance_message2').removeClass('hidden');
        }



    });

    return group;
};

var makeMutable = function(target) {
    target.set({
        lockMovementX:	false,
        lockMovementY:	false,
        lockScalingX:	false,
        lockScalingY:	false,
        lockRotation:	false,
        hasRotatingPoint: false,
        cornerSize: 3
    });
};

var mutableGroup = function (name, members) {
    var group = new fabric.Group(members,{
        'name': name,
        originY: 'top',
        lockMovementX:	false,
        lockMovementY:	false,
        lockScalingX:	false,
        lockScalingY:	false,
        lockRotation:	false,
        hasRotatingPoint: false,
        cornerSize: 3,
    });
    return group;
};


var updateFontSize = function (target, template, slidenum, maxwidth=textwidth()) {
    slides[slidenum].remove(target);

    var name    = target['name'];
    var text    = innerelements[name + ' text'];
    var box     = innerelements[name + ' box'];

    var alignment = innertheme[template][name]['alignment'];

    text.set({
        fontSize:   getFontSize(name)
    });
    wrapText(text, slidenum, maxwidth);
    box.set({
        left:   leftspace(),
        width:  maxwidth
    });
    setInnerBoxHeight(template, name);
    setInnerTextAlignment(template, name, alignment, slidenum);

    target = immutableGroup(name, [text,box]);
    addInnerTemplate(target, slidenum);
    setTypesOfTextInnerVerticalLayout();
};

var updateTemplateFont = function (target, entry, slidenum, maxwidth=textwidth()) {
    var name    = target['name'];
    var text    = innerelements[name + ' text'];
    var box     = innerelements[name + ' box'];

    var alignment = innertheme[entry][name]['alignment'];

    text.set({
        fontSize:   getFontSize(name),
        fontFamily: getFontFamily(name)
    });
    wrapText(text, slidenum, maxwidth);
    box.set({
        left:   leftspace(),
        width:  maxwidth
    });
    setInnerBoxHeight(entry, name);
    initialiseInnerTextAlignment(entry,name);
    // setInnerTextAlignment(entry, name, alignment, slidenum);

    slides[slidenum].remove(target);
    target = immutableGroup(name, [text,box], slidenum);
    addInnerTemplate(target, slidenum);
    setInnerTextAlignment(entry, name, alignment, slidenum);
};

var updateStructureFont = function (target) {
    var structure  = innertemplates[target];

    structure.set({
        fontSize:   getFontSize('structure'),
        fontFamily: getFontFamily('structure')
    });
    wrapText(structure, structure['slidenum'], textwidth());
};


var wrapText = function (template, slidenum, maxwidth=textwidth()) {
    var text        = template['text'].replace(/(\r\n|\n|\r)/gm, " ");
    var words       = text.split(" ");
    // var fontsize    = scalefactor*template['fontSize'];
    // var fontfamily  = scalefactor*template['fontFamily'];
    // maxwidth       *= scalefactor
    var fontsize    = template['fontSize'];
    var fontfamily  = template['fontFamily'];
    var context     = slides[slidenum].getContext("2d");

    context.font    = fontsize + "px " + fontfamily;

    var formatted   = '';
    var curr        = '';

    var n = 0;
    while (n < words.length) {
        var newline     = (curr == '')
        var testwidth   = curr + ' ' + words[n];

        var w           = context.measureText(testwidth).width;

        if (w < maxwidth) { // if not, keep adding words
            if (!newline) {
                curr   += ' ';
            }
            curr       += words[n];
            n++;
        } else if (newline) {   //Word length exceeds width on newline, then add anyway
            formatted  += words[n] + '\n';
            n++;
        } else {
            formatted  += curr + '\n';
            curr        = "";
        }
    }

    formatted += curr;

    // if (formatted.substr(formatted.length - 1) == "\n") {
    //     formatted = formatted.substr(0, formatted.length - 1);
    // }
    template.setText(formatted);
};


// function wrapText(slide, text, fontsize, fontfamily, maxW) {
//     var words = text.split(" ");
//     var formatted = '';
//
//     // clear newlines
//     var sansBreaks = text.replace(/(\r\n|\n|\r)/gm, "");
//     var context = slide.getContext("2d");
//     context.font = fontsize + "px " + fontfamily;
//     var curr = '';
//     n = 0;
//     while (n < words.length) {
//         var isnewline = (curr == '');
//         var testoverlap = curr + ' ' + words[n];
//
//         // are we over width?
//         var w = context.measureText(testoverlap).width;
//
//         if (w < maxW) { // if not, keep adding words
//             if (!isnewline) {
//             	curr += ' ';
//            	}
//             curr += words[n];
//             n++;
//         } else {
//             formatted += curr + '\n';
//             curr = "";
//         }
//     }
//
//     // get rid of empty newline at the end
//     formatted = formatted.substr(0, formatted.length - 1);
//     return formatted;
// };



/*
* =================================================================================================
*      Outer template functions
* =================================================================================================
*/
var outerBox = function () {
    var box = new fabric.Rect({
        originX: 'left',
        originY: 'top',
    });
    return box;
};


/*
* =================================================================================================
*      Inner template functions
* =================================================================================================
*/

var templateHeading = function(name, value, slidenum=0) {
    var heading = new fabric.Text(value, {
        'name':     name,
        'slidenum': slidenum,
        originY:    'top',
        originX:    'left',
        left:       leftspace(),
        fill:       getColor('structure', 'fg', colortheme['structure']['fg']),
        fontSize:	getFontSize('structure'),
        fontFamily: getFontFamily('structure'),
        textBackgroundColor: 'rgba(0,0,0,0)',
        selectable: false
    });

    headings[name] = heading;

    return heading;
};

var innerBox = function (name) {
    var box = new fabric.Rect({
        'name':     name,
        originX:    'left',
        originY:    'top',
        left:       leftspace(),
        width:      textwidth(),
    });
    innerelements[name] = box;
    return box;
};

var innerText = function (name, value) {
    var text = new fabric.Text(value, {
        'name':     name,
        originY:    'top',
        textBackgroundColor: 'rgba(0,0,0,0)',
    });

    innerelements[name] = text
    return text;
};

// var innerIText = function (name, value) {
//     var text = new fabric.IText(value, {
//         'name':     name,
//         originY:    'top',
//         textBackgroundColor: 'rgba(0,0,0,0)'
//     });
//     innerelements[name] = text
//     return text;
// };

// Box functions
var setInnerBoxHeight = function (template, name) {
    var entry   = innertheme[template][name];

    var sep;
    if ((opt = entry['vsep']) == 'empty') {
        sep = 0;
    } else {
        sep = ptToPx(entry[opt]);
    }

    var box     = innerelements[name + ' box'];
    var text    = innerelements[name + ' text'];

    var val = 2*sep + text['height'];

    box.set({
        height: val
    });
};

// Text functions
var initialiseInnerTextAlignment = function (template, name) {
    var entry       = innertheme[template][name];
    var alignment   = entry['alignment'];
    var hspace      = mmToPx(entry['hspace*']);
    var sep;
    if ((opt = entry['hsep']) == 'empty') {
        sep = 0;
    } else if (opt == 'sep'){
        sep = ptToPx(entry[opt]);
    } else if (opt == 'colsep*') {
        sep = emToPx(entry[opt]);
    } else {
        sep = 0;
        console.log('hsep error - ', template, name, opt);
    }

    var text = innerelements[name + ' text'];
    var val = 0;
    switch (alignment) {
        case 'left':
        val = leftspace() + sep + hspace;
        break;
        case 'center':
        val = leftspace() + (textwidth()/2) + hspace;
        break;
        case 'right':
        val = paperwidth() - rightspace() - sep;
        break;
        default:
        console.log("ALIGNMENT PROPERTY OF", template, ' - ', name, ' NOT SET');
        break;
    }

    text.set({
        originX:    alignment,
        left:       val
    });
};

var setInnerTextAlignment = function (template, name, alignment, slidenum) {
    var entry   = innertheme[template][name];
    var hspace  = mmToPx(entry['hspace*']);
    var sep;
    if ((opt = entry['hsep']) == 'empty') {
        sep = 0;
    } else {
        sep = ptToPx(entry[opt]);
    }
    var box     = innerelements[name + ' box'];
    var text    = innerelements[name + ' text'];
    var val     = 0;

    switch (alignment) {
        case 'left':
        val = box['left'] + sep + hspace;
        break;
        case 'center':
        val = hspace;
        break;
        case 'right':
        val = box['width']/2 - sep;// + hspace; NO CHANGE IN BEAMER
        break;
        default:
        console.log('ALIGNMENT SPECIFICATION ERROR - ', template, name, alignment);
        break;
    }

    entry['alignment'] = alignment;
    text.set({
        originX:    alignment,
        left:       val
    });

    slides[slidenum].renderAll();
};

var setInnerTextTop = function (template, name) {
    var entry   = innertheme[template][name];
    var sep;
    if ((opt = entry['vsep']) == 'empty') {
        sep = 0;
    } else {
        sep = ptToPx(entry[opt]);
    }
    var box     = innerelements[name + ' box'];
    var text    = innerelements[name + ' text'];

    var val     = box['top'] + sep;

    text.set({
        top:       val
    });
};

var setTemplateShadow = function(target, rounded, shadow, override='empty') {
    var shadoweffect = {
       color: 'rgba(0,0,0,0.6)',
       blur: 10,
       offsetX: 5,
       offsetY: 5,
       opacity: 0.6,
       fillShadow: true,
       strokeShadow: true
    }

    if (rounded == 'true' && shadow == 'true') {
        if (override == 'empty') {
            override = target;
        }
        if (colortheme[override]['bg']['type'].indexOf('color') != -1) {
            innerelements[target + ' box'].set({
                'shadow':   shadoweffect
            });
        }
    } else {
        innerelements[target + ' box'].set({
            'shadow':   ''
        });
    }
};
