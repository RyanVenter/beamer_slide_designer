$('#thumbnails a').on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {

        // Prevent default anchor click behavior
        event.preventDefault();

        var curr = window.scrollY;
        var hash = this.hash;
        var offset = parseInt($('body').css('padding-top'));
        var target = $(hash).offset().top - offset;
        var t = Math.abs(target - curr);
        // console.log(curr, hash, offset, target, t);

        // Using jQuery's animate() method to add smooth page scroll
        $('html, body').animate({
            scrollTop: target
        }, t, function() {
            window.location.hash = hash;
        });
    } else {
        console.log("check");
    }
});
