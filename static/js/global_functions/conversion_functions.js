/*
 * To PX FUNCTIONS
 */
 var mmToPx = function (num) {
     return +(num*3.779528).toFixed(2);
 };

 var ptToPx = function (num) {
     return +(num*1.333333).toFixed(2);
 };


/*
 * From PX FUNCTIONS
 */

var pxToMm = function (num) {
    return +(num*0.264583).toFixed(2);
};

var pxToPt = function (num) {
    return +(num*0.75).toFixed(0);
};


// FUNCTIONS OF EM

var emToMm = function (num) {
        return +(num*4.2175176).toFixed(2);
};

var mmToEm = function (num) {
    return +(num*0.237106301584).toFixed(2);
};

var emToPx = function (num) {
    return mmToPx(emToMm(num));
}

var pxToEm = function (num) {
    return mmToEm(pxToMm(num));
}
