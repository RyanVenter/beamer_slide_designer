var openProject = function(path, fname) {
    $.ajax({
        type: 'POST',
        url: $SCRIPT_ROOT+"/open_project",
        contentType: 'application/json',
        data: JSON.stringify({
            'path': path,
            'fname': fname
        }),
        dataType: 'json',
        success: function (ret) {
            if (ret['status'] == 'new') {
                alert('New Project');
            } else {
                alert('Configuring existing settings');
                var rescale = (config['aspectratio'] != ret['config']['aspectratio'])
                config      = $.extend(deep=true, {}, ret['config']);
                colortheme  = $.extend(deep=true, {}, ret['color']);
                fonttheme   = $.extend(deep=true, {}, ret['font']);
                innertheme  = $.extend(deep=true, {}, ret['inner']);
                outertheme  = $.extend(deep=true, {}, ret['outer']);
                if (rescale) {
                    scaleToAspectRatio();
                }
                refreshSlides();
            }
        }
    });
};

var openPlayground = function() {
    $('#project_id').html('playground');
    $('#load_existing').addClass('hidden');
    $('#load_existing_id').addClass('hidden');
    $('#save').addClass('hidden');
    $('#download').addClass('hidden');
};

$('#open_playground_button').on('mousedown', function() {
    openPlayground();
    $('#set_project').modal('hide');

});

$('#set_project')
    .modal({
        blurring: true,
    })
    .modal('setting', {
        closable: false,
        onApprove : function() {
            var username = JSON.stringify($('#username').val()).replace(/[^0-9a-z]/gi, '');
            var project = JSON.stringify($('#project_name').val()).replace(/[^0-9a-z]/gi, '');
            if (username == '') { username = 'default'; };
            if (project == '') { project = 'default'; };

            var project_path = username + '/' + project;
            var project_fname = username + '_' + project;
            var project_source = project_path + '/' + project_fname;


            if (username == 'default') {
                project_path = 'playground';
            }
            $('#project_id').html(project_fname);
            $('#project_id').attr('path', project_path);
            $('#project_id').attr('fname', project_fname);

            if (project_path != 'playground') {
                $('#download').attr('href', $PROJECTS_ROOT + project_path + '/bsd-theme-package.zip');
                $('#download').attr('download', project_fname+'.zip');
                openProject(project_path, project_fname);
            } else {
                openPlayground();
            }
        }
    })
    .modal('show')
;

$('#loader')
    .modal({
        blurring: true,
    })
    .modal('hide')
;
