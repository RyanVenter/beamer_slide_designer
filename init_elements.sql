insert into elements (name, bg, slide)
values ('canvas', '#FFFFFF', 'presentation');


/* **********************************************************************************
 * *** BLOCKS *********************************************************************** * ******************************************************************************* */

-- FRAME ELEMENTS

insert into elements (name, text, fg, bg, fontSize, slide)
values ('blocks frame title', 'Types of Blocks', '#3434B4', '#FFFFFF', 20, 'blocks');

insert into elements (name, text, fg, bg, fontSize, slide)
values ('block title', 'Normal block', '#3434B4', '#FFFFFF', 15, 'blocks');

insert into elements (name, text, fg, bg, fontSize, slide)
values ('block body', 'The quick brown fox jumps over the lazy dog', '#000000', '#FFFFFF', 15, 'blocks');

insert into elements (name, text, fg, bg, fontSize, slide)
values ('block title alerted', 'Alerted block', '#FF0000', '#FFFFFF', 15, 'blocks');

insert into elements (name, text, fg, bg, fontSize, slide)
values ('block body alerted', 'The quick brown fox jumps over the lazy dog', '#000000', '#FFFFFF', 15, 'blocks');

insert into elements (name, text, fg, bg, fontSize, slide)
values ('block title example', 'Example block', '#007900', '#FFFFFF', 15, 'blocks');

insert into elements (name, text, fg, bg, fontSize, slide)
values ('block body example', 'The quick brown fox jumps over the lazy dog', '#000000', '#FFFFFF', 15, 'blocks');

