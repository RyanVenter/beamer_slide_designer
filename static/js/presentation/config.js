
// default_c onfig = {
//     "aspectratio" :         "43",
//     "basefontsize":         11,     //measured in pt
//     "text margin left":     10,     //measured in mm
//     "text margin right":    10,     //measured in mm
//     "sidebar width left":   0,      //measured in mm
//     "sidebar width right":  0,      //measured in mm
//     // "description width"
//     // "description width of"
//     "slidenum":             0,
//
// }
var config = $.extend(deep=true, {}, default_config);
var colortheme = $.extend(deep=true, {}, default_colortheme);
var fonttheme = $.extend(deep=true, {}, default_fonttheme);
var innertheme = $.extend(deep=true, {}, default_innertheme);
var outertheme = $.extend(deep=true, {}, default_outertheme);

var paperwidth = function() {
    return mmToPx(dimensions[config['aspectratio']]['paperwidth']);
};

var paperheight = function() {
    return mmToPx(dimensions[config['aspectratio']]['paperheight']);
};

var paperratio = function(num, denom) {
    return (dimensions[config['aspectratio']]['ratio'+num]/dimensions[config['aspectratio']]['ratio'+denom]);
};

var leftspace = function() {
    return mmToPx(config['text margin left'] + config['sidebar width left']);
};

var rightspace = function() {
    return mmToPx(config['text margin right'] + config['sidebar width right']);
};

var textwidth = function() {
    return paperwidth() - leftspace() - rightspace();
};

var textheight = function() {
    return paperheight();//slides[0]['height'];
}


var thumbwidth = 160;   //pixels

var outertemplates = {};
var outerelements = {};

var innertemplates = {};
var innerelements = {};

var headings = {};

var slides = [];
var thumbs = [];

var active_template = "";
var active_slide    = 0;
var scalefactor     = 1;
