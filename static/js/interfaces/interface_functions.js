var refreshSlides = function() {
    outertemplates['background canvas'].set({
        fill: getColor('background canvas', 'bg', colortheme['background canvas']['bg'])
    });
    for (var i in slides) {
        slides[i].refreshFontsFunction('empty');
        slides[i].refreshColorsFunction();
        slides[i].refreshShadowsFunction();
    }
    refreshToolbarColorFunctions();
};

var refreshSlideFonts = function() {
    for (var i in slides) {
        slides[i].refreshFontsFunction('empty');
    }
};

var refreshSlideColors = function() {
    outertemplates['background canvas'].set({
        fill: getColor('background canvas', 'bg', colortheme['background canvas']['bg'])
    });
    for (var i in slides) {
        slides[i].refreshColorsFunction('empty');
    }
    refreshToolbarColorFunctions();
};

/* Dropdown elements */
var dropdownElem = function(elem) {
    elem.dropdown();
};

$('[class*="dropdown"]').each(function() {
    new dropdownElem($(this));
});
