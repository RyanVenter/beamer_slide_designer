
/*
 * NOTE: outerthemetemplates.js must be in scope
 */

var scaleThumb = function (target, width,height,scaled_width) {
    factor = scaled_width/width;

    target.setHeight(height * factor);
    target.setWidth(width * factor);
    // if (target.backgroundImage) {
    //     // Need to scale background images as well
    //     var bi = target.backgroundImage;
    //     bi.width = bi.width * factor; bi.height = bi.height * factor;
    // }
    var objects = target.getObjects();
    for (var i in objects) {
        var scaleX = objects[i].scaleX;
        var scaleY = objects[i].scaleY;
        var left = objects[i].left;
        var top = objects[i].top;

        var tempScaleX = scaleX * factor;
        var tempScaleY = scaleY * factor;
        var tempLeft = left * factor;
        var tempTop = top * factor;

        objects[i].scaleX = tempScaleX;
        objects[i].scaleY = tempScaleY;
        objects[i].left = tempLeft;
        objects[i].top = tempTop;

        objects[i].setCoords();
    }
    target.renderAll();
    target.calcOffset();
};

var scaleTemplate = function(template, factor) {
    var scaleX = template.scaleX;
    var scaleY = template.scaleY;
    var left = template.left;
    var top = template.top;

    var tempscaleX = scaleX * factor;
    var tempscaleY = scaleY * factor;
    var templeft = left * factor;
    var temptop = top * factor;

    template.scaleX = tempscaleX;
    template.scaleY = tempscaleY;
    template.left = templeft;
    template.top = temptop;

    template.setCoords();

    scalefactor = tempscaleY;
};

var stretchTemplate = function(template, wfactor, hfactor) {
    // var scaleX = template.scaleX;
    // var scaleY = template.scaleY;
    // var left = template.left;
    // var top = template.top;
    //
    // var tempscaleX = scaleX * wfactor;
    // var tempscaleY = scaleY * hfactor;
    // var templeft = left * wfactor;
    // var temptop = top * wfactor;

    template.scaleX = 1;
    template.scaleY = 1;
    // template.left = templeft;
    // template.top = temptop;

    template.setCoords();

    template.set({
        width: slides[0].getWidth(),
        height: slides[0].getHeight()
    })

    // scalefactor = tempscaleY;
};

var addInnerTemplate = function(template, slidenum) {
    innertemplates[template['name']] = template;
    var factor = slides[0].getWidth()/paperwidth();
    scaleTemplate(template, factor);

    slides[slidenum].add(template);
    slides[slidenum].renderAll();
}

var scaleSlides = function (slide_width, slide_height, wfactor, hfactor) {
    for (var i in outertemplates) {
        scaleTemplate(outertemplates[i], wfactor);
    }

    // stretchTemplate(outertemplates['background canvas'], wfactor, hfactor)

    for (var i in innertemplates) {
        scaleTemplate(innertemplates[i], wfactor);
    }

    for (var i in slides) {
        slides[i].setWidth(slide_width);
        slides[i].setHeight(slide_height);
        slides[i].renderAll();
        slides[i].calcOffset();
    }
    stretchTemplate(outertemplates['background canvas'], wfactor, hfactor)
};

var scaleAll = function() {
    var factor = $('#zoom').val();

    var width_offset = parseInt($('#one').css('width')) + parseInt($('#two').css('width'));
    var max_width = window.innerWidth - width_offset;

    var height_offset = parseInt($('body').css('padding-top'));
    var max_height = window.innerHeight - height_offset;

    var slide_height = (max_width*paperratio('height','width'));
    var slide_width = max_width;

    if (slide_height > max_height) {
        slide_height = max_height;
        slide_width = slide_height*paperratio('width','height');
    }

    slide_height *= factor;
    slide_width *= factor;

    scaleSlides(slide_width, slide_height, slide_width/slides[0].getWidth(), slide_height/slides[0].getHeight());

    $('#slides').css('padding-bottom',(max_height-slide_height+5)+'px');
};

var scaleToAspectRatio = function() {
    slide_height = paperheight();
    slide_width = paperwidth();
    scaleSlides(slide_width, slide_height, slide_width/slides[0].getWidth(), slide_height/slides[0].getHeight());
    scaleAll();

}


var scaleNewSlide = function(slide) {
    var factor = $('#zoom').val();

    var width_offset = parseInt($('#one').css('width')) + parseInt($('#two').css('width'));
    var max_width = window.innerWidth - width_offset;

    var height_offset = parseInt($('body').css('padding-top'));
    var max_height = window.innerHeight - height_offset;

    var slide_height = (max_width*paperratio('height','width'));
    var slide_width = max_width;

    if (slide_height > max_height) {
        slide_height = max_height;
        slide_width = slide_height*paperratio('width','height');
    }

    slide_height *= factor;
    slide_width *= factor;

    slide.setWidth(slide_width);
    slide.setHeight(slide_height);
    slide.renderAll();
    slide.calcOffset();
};

var slideScript = function(num) {
        return "<div id=\"slide"+num+"\">\n\t<canvas id=\"canvas"+num+"\" width=\""+paperwidth()+"\" height=\""+paperheight()+"\"></canvas>\n</div>";
};

var thumbScript = function(num, inject) {
    return "<li"+inject+">\n\t<a class=\"thumbnail\" href=\"#slide"+num+"\">\n\t\t<canvas id=\"thumb"+num+"\" width=\""+thumbwidth+"\"></canvas>\n\t\t"+(num+1)+"\n\t</a>\n</li>";
};

var addSlide = function(name="") {
        $("#slides").append(slideScript(config['slidenum']));

        var inject = "";
        if (config['slidenum'] == 0) {
            inject = " class=active";
        }
        $("#thumbs").append(thumbScript(config['slidenum'], inject));

        var target = "canvas"+config['slidenum'];
        var index = config['slidenum'];

        slide = new fabric.Canvas(target, {
                backgroundColor: 'rgb(255,255,255)', //getColor('background canvas', 'bg'),
                selectionColor: 'rgb(230,230,230)',
                selectionWidth: 3,
                'name': name
            });
        slide['slidenum'] = ""+index;

        for (var i in outertemplates) {
            slide.add(outertemplates[i]);
        }

        slides.push(slide);
        //INEFFICIENT SOLUTION
        scaleAll();

        var target = "thumb"+config['slidenum'];
        thumb = new fabric.StaticCanvas(target);
        thumb.loadFromJSON(JSON.stringify(slide));
        scaleThumb(thumb, paperwidth(), paperheight(), thumbwidth);
        thumb['slidenum'] = ""+index;


        thumbs.push(thumb);

        slides[index].on('after:render', function() {
            thumbs[index].loadFromJSON(JSON.stringify(slides[index]));
            scaleThumb(thumbs[index], slides[index].getWidth(), slides[index].getHeight(), thumbwidth);
        });

        config['slidenum'] += 1
        return slide['slidenum'];
};

var padSlideInterface = function() {
    var height_offset = parseInt($('body').css('padding-top'));
    var max_height = window.innerHeight - height_offset;
    $('#slides').css('padding-bottom',(max_height+5)+'px');
};
$(document).ready(padSlideInterface());

$(window).on('resize', function() {
    scaleAll();
});

$('#zoom').on('input', function() {
    scaleAll();
});


var loadFonts = function() {
    var text = new fabric.Text('test');

    var series  = ['','-bfseries'];
    var shape   = ['','-itshape','-slshape','-scshape'];
    for (var family in fontfamilies) {
        for (var name in fontfamilies[family]) {
            var font = fontfamilies[family][name]['entry'];
            for (var i in series) {
                for (var j in shape) {
                    var val = font + series[i] + shape[j];
                    text.set({fontFamily: val});
                }
            }
        }
    }
}
$(document).ready(loadFonts());
