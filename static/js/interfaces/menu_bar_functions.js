/*
 * ================================================================================================
 *     Load existing project
 * ================================================================================================
*/
$('#load_existing_id').on('mousedown', function() {
    $('#load_existing_id').html('');
});

$('#load_existing').on('mousedown', function() {
    var target      = ''
    var components  = $('#load_existing_id').val().split(':');
    if (components.length == 2) {
        target      = components[1].replace(/[^0-9a-z]/gi, '');
    }
    var id          = components[0].split('_');
    if (id.length != 2) {
        if (id.length == 1 && id[0] == 'default') {
            if (target == 'color') {
                colortheme  = $.extend(deep=true, {}, default_colortheme);
                refreshSlideColors();
            } else if (target == 'font') {
                fonttheme   = $.extend(deep=true, {}, default_fonttheme);
                refreshSlideFonts();
            } else if (target == 'inner') {
                innertheme  = $.extend(deep=true, {}, default_innertheme);
                refreshSlideFonts();
            } else if (target == 'outer') {
                outertheme  = $.extend(deep=true, {}, default_outertheme);
                refreshSlides();
            } else {
                var rescale = (config['aspectratio'] != default_config['aspectratio'])
                config      = $.extend(deep=true, {}, default_config);
                colortheme  = $.extend(deep=true, {}, default_colortheme);
                fonttheme   = $.extend(deep=true, {}, default_fonttheme);
                innertheme  = $.extend(deep=true, {}, default_innertheme);
                outertheme  = $.extend(deep=true, {}, default_outertheme);
                if (rescale) {
                    scaleToAspectRatio();
                }
                refreshSlides();
            }
        } else {
            $('#load_existing_id').val('Invalid ID: try again');
        }
    } else {
        var username    = id[0].replace(/[^0-9a-z]/gi, '');
        var projectname = id[1].replace(/[^0-9a-z]/gi, '');
        var fname       = username + '_' + projectname;
        if (fname == $('#project_id').val()) {
            $('#load_existing_id').val('Good one!');
        } else {
            var validoptions = ['','color','font','inner','outer'];
            if (validoptions.indexOf(target) != -1) {
                $.ajax({
                    type: 'POST',
                    url: $SCRIPT_ROOT+"/load_existing",
                    contentType: 'application/json',
                    data: JSON.stringify({
                        'fname': fname,
                        'path': $('#project_id').attr('path'),
                        'target': target
                    }),
                    dataType: 'json',
                    success: function (ret) {
                        if (ret['status'] == '404') {
                            alert('The requested project was not found');
                        } else if (ret['status'] == 'private'){
                            alert('Access denied: ' + fname + ' is flagged as private')
                        } else {
                            alert('Configuring loaded settings');
                            $('#load_existing_id').html('loaded from:' + fname);
                            if (target == 'color') {
                                colortheme  = $.extend(deep=true, {}, ret['color']);
                                refreshSlideColors();
                            } else if (target == 'font') {
                                fonttheme   = $.extend(deep=true, {}, ret['font']);
                                refreshSlideFonts();
                            } else if (target == 'inner') {
                                innertheme  = $.extend(deep=true, {}, ret['inner']);
                                refreshSlideFonts();
                            } else if (target == 'outer') {
                                outertheme  = $.extend(deep=true, {}, ret['outer']);
                                refreshSlides();
                            } else {
                                var rescale = (config['aspectratio'] != ret['config']['aspectratio'])
                                config      = $.extend(deep=true, {}, ret['config']);
                                colortheme  = $.extend(deep=true, {}, ret['color']);
                                fonttheme   = $.extend(deep=true, {}, ret['font']);
                                innertheme  = $.extend(deep=true, {}, ret['inner']);
                                outertheme  = $.extend(deep=true, {}, ret['outer']);
                                if (rescale) {
                                    scaleToAspectRatio();
                                }
                                refreshSlides();
                            }
                        }

                    }
                });
            } else {
                $('#load_existing_id').val('Invalid target: try again');
            }
        }
    }
});


/*
 * ================================================================================================
 *     Save
 * ================================================================================================
*/

// $('#saved_message')
//     .modal({
//         blurring: true,
//     })
//     .modal('hide')
// ;

$("#save").on('mousedown', function() {
    $.ajax({
        url: $SCRIPT_ROOT+"/save",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            'keys'      : Object.keys(colortheme),
            'config'    : config,
            'color'     : colortheme,
            'font'      : fonttheme,
            'inner'     : innertheme,
            'outer'     : outertheme,
            'id': {
                'path'  : $('#project_id').attr('path'),
                'fname' : $('#project_id').attr('fname')
            },
        }),
        dataType: 'json',
        success: function (ret) {
            alert('Project settings have been saved successfully!')
            console.log('JSON posted: ' + JSON.stringify(ret));
        }
    });
});

/*
 * ================================================================================================
 *     Download
 * ================================================================================================
*/
