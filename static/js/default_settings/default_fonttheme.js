var fontsizes = {
    8:  {
        tiny:           5, //measured in pt
        scriptsize:     5,
        footnotesize:   6,
        small:          7,
        normalsize:     8,
        large:          10,
        Large:          11,
        LARGE:          12,
        huge:           14,
        Huge:           17
    },

    9:  {
        tiny:           5, //measured in pt
        scriptsize:     6,
        footnotesize:   7,
        small:          8,
        normalsize:     9,
        large:          10,
        Large:          11,
        LARGE:          12,
        huge:           14,
        Huge:           17
    },

    10:  {
        tiny:           5, //measured in pt
        scriptsize:     7,
        footnotesize:   8,
        small:          9,
        normalsize:     10,
        large:          12,
        Large:          14,
        LARGE:          17,
        huge:           20,
        Huge:           25
    },

    11:  {
        tiny:           6, //measured in pt
        scriptsize:     8,
        footnotesize:   9,
        small:          10,
        normalsize:     11,
        large:          12,
        Large:          14,
        LARGE:          17,
        huge:           20,
        Huge:           25
    },

    12:  {
        tiny:           6, //measured in pt
        scriptsize:     8,
        footnotesize:   10,
        small:          11,
        normalsize:     12,
        large:          14,
        Large:          17,
        LARGE:          20,
        huge:           25,
        Huge:           25
    },

    14:  {
        tiny:           6, //measured in pt
        scriptsize:     8,
        footnotesize:   10,
        small:          12,
        normalsize:     14,
        large:          17,
        Large:          20,
        LARGE:          25,
        huge:           29.86,
        Huge:           35.83
    },

    17:  {
        tiny:           8, //measured in pt
        scriptsize:     10,
        footnotesize:   12,
        small:          14,
        normalsize:     17,
        large:          20,
        Large:          25,
        LARGE:          29.86,
        huge:           35.83,
        Huge:           42.99
    },

    20:  {
        tiny:           10, //measured in pt
        scriptsize:     12,
        footnotesize:   14,
        small:          17,
        normalsize:     20,
        large:          25,
        Large:          29.86,
        LARGE:          35.83,
        huge:           42.99,
        Huge:           51.59
    }
};

var fontfamilies = {
    'sf': {
        'computer-modern': {
            'name':     'Computer Modern',
            'entry':    'computer-modern-sf',
            'package':  'default'
        }

    },


    'rm': {
        'computer-modern': {
            'name':     'Computer Modern',
            'entry':    'computer-modern-rm',
            'package':  'default',
        },

        'accanthis': {
            'name':     'Accanthis',
            'entry':    'accanthis-rm',
            'package':  'accanthis'
        },

        'libre-baskerville': {
            'name':     'Libre Baskerville',
            'entry':    'libre-baskerville-rm',
            'package':  'librebaskerville'
        },

        'libre-caslon': {
            'name':     'Libre Caslon',
            'entry':    'libre-caslon-rm',
            'package':  'librecaslon'
        },

        'new-px': {
            'name':     'New Px',
            'entry':    'new-px-rm',
            'package':  'lnewpxtext,newpxmath'
        }


    },

    'tt': {
        'computer-modern': {
            'name':     'Computer Modern',
            'entry':    'computer-modern-tt',
            'package':  'default',
        }
    }
};


var default_fonttheme = {
    'familydefault':        'sffamily',

    'mathfamilydefault':    'sffamily',

    'sffamily': {
        'name':     'Computer Modern',
        'entry':    'computer-modern-sf',
        'package':  'default',
    },

    'rmfamily': {
        'name':     'Computer Modern',
        'entry':    'computer-modern-rm',
        'package':  'default',
    },

    'ttfamily': {
        'name':     'Computer Modern',
        'entry':    'computer-modern-tt',
        'package':  'default',
    },

    'normal text': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'alerted text': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'example text': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'structure': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'tiny structure': {
        parent:     'empty',
        size:       'tiny',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'title': {
        parent:     'structure',
        size:       'Large',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'title in head/foot': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'title in sidebar': {
        parent:     'empty',
        size:       'tiny',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subtitle': {
        parent:     'title',
        size:       'normalsize',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'author': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'author in head/foot': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },


    'author in sidebar': {
        parent:     'empty',
        size:       'tiny',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'institute': {
        parent:     'empty',
        size:       'scriptsize',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'institute in head/foot': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },


    'institute in sidebar': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'date': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'date in head/foot': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },


    'date in sidebar': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'part name': {
        parent:     'empty',
        size:       'LARGE',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'part title': {
        parent:     'title',
        size:       'LARGE',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'section name': {
        parent:     'empty',
        size:       'Large',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'section title': {
        parent:     'title',
        size:       'Large',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'section in toc': {
        parent:     'structure',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'section in toc shaded': {
        parent:     'section in toc',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'section in head/foot': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'section in sidebar': {
        parent:     'empty',
        size:       'tiny',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'section number projected': {
        parent:     '{section in toc,projected text}',
        size:       'small',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsection name': {
        parent:     'empty',
        size:       'large',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsection title': {
        parent:     'title',
        size:       'large',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsection in toc': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsection in toc shaded': {
        parent:     'subsection in toc',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsection in head/foot': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsection in sidebar': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsubsection in toc': {
        parent:     'empty',
        size:       'footnotesize',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsubsection in toc shaded': {
        parent:     'subsubsection in toc',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsubsection in head/foot': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsubsection in sidebar': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'headline': {
        parent:     'tiny structure',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'footline': {
        parent:     'tiny structure',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'sidebar': {
        parent:     'tiny structure',
        size:       'tiny',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'sidebar left': {
        parent:     'sidebar',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'sidebar right': {
        parent:     'sidebar',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'frametitle': {
        parent:     'structure',
        size:       'Large',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'framesubtitle': {
        parent:     'frametitle',
        size:       'footnotesize',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'caption': {
        parent:     'empty',
        size:       'small',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'caption name': {
        parent:     '{structure,caption}',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'button': {
        parent:     'empty',
        size:       'tiny',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'block body': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'block body alerted': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'block body example': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'block title': {
        parent:     '{structure,block body}',
        size:       'large',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'block title alerted': {
        parent:     '{block title,alerted text}',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'block title example': {
        parent:     '{block title,example text}',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'item': {
        parent:     'structure',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subitem': {
        parent:     'item',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsubitem': {
        parent:     'subitem',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'item projected': {
        parent:     '{item,projected text}',
        size:       'tiny',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subitem projected': {
        parent:     'item projected',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'subsubitem projected': {
        parent:     'subitem projected',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'itemize item': {
        parent:     'item',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'itemize subitem': {
        parent:     'subitem',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'itemize subsubitem': {
        parent:     'subsubitem',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'enumerate item': {
        parent:     'item',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'enumerate subitem': {
        parent:     'subitem',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'enumerate subsubitem': {
        parent:     'subsubitem',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'itemize/enumerate body': {
        parent:     'empty',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'itemize/enumerate subbody': {
        parent:     'empty',
        size:       'small',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'itemize/enumerate subsubbody': {
        parent:     'empty',
        size:       'footnotesize',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'description item': {
        parent:     'item',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'projected text': {
        parent:     'tiny structure',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'abstract': {
        parent:     'empty',
        size:       'small',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'abstract title': {
        parent:     '{abstract,structure}',
        size:       'normalsize',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'verse': {
        parent:     'empty',
        size:       'empty',
        shape:      'itshape',
        series:     'empty',
        family:     'rmfamily'
    },

    'quotation': {
        parent:     'empty',
        size:       'empty',
        shape:      'itshape',
        series:     'empty',
        family:     'empty'
    },

    'quote': {
        parent:     'quotation',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'note page': {
        parent:     'empty',
        size:       'small',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'note title': {
        parent:     'note page',
        size:       'empty',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },

    'note date': {
        parent:     'empty',
        size:       'footnotesize',
        shape:      'empty',
        series:     'empty',
        family:     'empty'
    },























}
