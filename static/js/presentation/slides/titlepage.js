/*
* =================================================================================================
*      Title page
* =================================================================================================
*/
var titlepagenum = addSlide('title page'); //Title page slide


// Title page functions
/* ---- Vertical inner layout ------------------------------------------------------------------ */
var setTitlePageInnerVerticalLayout = function () {
    var gap = 3;

    var entry       = innertheme['title page'];
    var boxheights  = title['height'] + subtitle['height'] + author['height'] + institute['height'] + date['height'];
    var gaps        = 3 * gap;
    var vskips      = emToPx(entry['subtitle']['vskip'] + entry['author']['vskip']+ entry['institute']['vskip']);

    var topmargin   = scalefactor*((textheight() - (boxheights + vskips + gaps))/2);

    title.set({
        top: topmargin
    });
    setSubtitleTop();
    setTitlePageTemplateTop('author', 'subtitle', gap);
    setTitlePageTemplateTop('institute', 'author',gap);
    setTitlePageTemplateTop('date', 'institute', gap);
    slides[titlepagenum].renderAll();
};

/* ---- Set title page template top attr ------------------------------------------------------- */
var setTitlePageTemplateTop = function (template, prev, gap) {
    var factor  = scalefactor; //title['scaleY'];
    var vskip   = emToPx(innertheme['title page'][prev]['vskip']);
    var height  = innertemplates[prev]['height'];

    var val     = innertemplates[prev]['top'] + title['scaleY']*(height + vskip + gap);
    innertemplates[template].set({
        top: val
    });
};

/* ---- Refresh slide fonts --------------------------------------------------------------------- */
var refreshTitlePageFonts = function (name) {
    updateTitleFont();
    updateSubtitleFont();
    updateTemplateFont(innertemplates['author'],'title page', titlepagenum);
    updateTemplateFont(innertemplates['institute'],'title page', titlepagenum);
    updateTemplateFont(innertemplates['date'],'title page', titlepagenum);

    setTitlePageInnerVerticalLayout();
    if (name != 'empty') {
        slides[titlepagenum].setActiveObject(innertemplates[name]);
    }
    slides[titlepagenum].renderAll();
};

/* ---- Refresh slide colors -------------------------------------------------------------------- */
var refreshTitlePageColors = function () {
    var templates = ['title', 'subtitle', 'author', 'institute', 'date'];
    for (var i in templates) {
        var name = templates[i]
        innerelements[name + ' text'].set({
            fill:   getColor(name, 'fg', colortheme[name]['fg'])
        });

        innerelements[name + ' box'].set({
            fill:   getColor(name, 'bg', colortheme[name]['bg'])
        });
    }

    innerelements['subtitle box'].set({
        fill:   innerelements['title box']['fill']
    });

    // slides[titlepagenum].renderAll();
    refreshTitlePageShadows();
};

/* ---- Refresh slide shadows ------------------------------------------------------------------- */
var refreshTitlePageShadows = function () {
    var templates = ['title', 'author', 'institute', 'date'];
    for (var i in templates) {
        var name = templates[i]
        setTemplateShadow(templates[i], innertheme['title page'][templates[i]]['rounded'],innertheme['title page'][templates[i]]['shadow']);
    }
    setTemplateShadow('subtitle', innertheme['title page']['title']['rounded'],innertheme['title page']['title']['shadow'], 'title');
    slides[titlepagenum].renderAll();
};

/* ---- Attach update function to slide -------------------------------------------------------- */
slides[titlepagenum].refreshFontsFunction = function (name) { refreshTitlePageFonts(name); };
slides[titlepagenum].refreshColorsFunction = function () { refreshTitlePageColors(); };
slides[titlepagenum].refreshShadowsFunction = function () { refreshTitlePageShadows(); };


/*
* ------------------------------------------------------------------------------------------------
*      Title
* ------------------------------------------------------------------------------------------------
*/

var updateTitleFont = function () {
    var template    = innertemplates['title'];
    var text        = innerelements['title text'];
    var box         = innerelements['title box'];
    var alignment   = innertheme['title page']['title']['alignment'];


    text.set({
        fontSize:   getFontSize('title'),
        fontFamily: getFontFamily('title')
    });
    wrapText(text, titlepagenum);
    box.set({
        left:   leftspace(),
        width:  textwidth()
    });

    setTitleBoxHeight(true);
    // setInnerTextTop('title page', 'title');
    initialiseInnerTextAlignment('title page', 'title');
    // setInnerTextAlignment('title page', 'title', alignment, titlepagenum);


    slides[titlepagenum].remove(title);
    title = immutableGroup('title', [text, box], titlepagenum);
    addInnerTemplate(title, titlepagenum);
    setInnerTextAlignment('title page', 'title', alignment, titlepagenum);

};

var setTitleTextAlignment = function (alignment) {
        setInnerTextAlignment('title page', 'title', alignment, titlepagenum);
        setInnerTextAlignment('title page', 'subtitle', alignment, titlepagenum);
};

// Title box functions
var setTitleBoxHeight = function (sub=true) {
    var sep = ptToPx(innertheme['title page']['title']['sep']);

    var multiplier = 2;
    if (sub) { multiplier = 1; }

    var val = multiplier*sep + title_text['height'];

    title_box.set({
        height: val
    });
};

// Title box
var title_box = innerBox('title box');
title_box.set({
    fill:      getColor('title', 'bg', colortheme['title']['bg']),
});

// Title text
var title_text = innerText('title text', innertheme['title page']['title']['text']);
title_text.set({
    fill:       getColor('title', 'fg', colortheme['title']['fg']),
    fontSize:	getFontSize('title'),
    fontFamily: getFontFamily('title'),
});

setTitleBoxHeight(true);
setInnerTextTop('title page', 'title');
initialiseInnerTextAlignment('title page', 'title');

// Title
var title = immutableGroup('title', [title_text, title_box], titlepagenum);
addInnerTemplate(title, titlepagenum);

/*
* ------------------------------------------------------------------------------------------------
*      Subtitle
* ------------------------------------------------------------------------------------------------
*/

var updateSubtitleFont = function () {
    var template    = innertemplates['subtitle'];
    var text        = innerelements['subtitle text'];
    var box         = innerelements['subtitle box'];
    var alignment   = innertheme['title page']['subtitle']['alignment'];


    text.set({
        fontSize:   getFontSize('subtitle'),
        fontFamily: getFontFamily('subtitle')
    });
    wrapText(text, titlepagenum);
    box.set({
        left:   leftspace(),
        width:  textwidth()
    });

    setSubtitleBoxHeight();
    // setInnerTextTop('title page', 'title');
    initialiseInnerTextAlignment('title page', 'subtitle');
    // setInnerTextAlignment('title page', 'title', alignment, titlepagenum);


    slides[titlepagenum].remove(subtitle);
    subtitle = immutableGroup('subtitle', [text, box], titlepagenum);
    addInnerTemplate(subtitle, titlepagenum);
    setInnerTextAlignment('title page', 'subtitle', alignment, titlepagenum);
};

// Subtitle functions
var setSubtitleTop = function () {
    var val = title['top'] + title['scaleY']*title['height'];
    subtitle.set({
        top: val
    });
};

// Subtitle box functions
var setSubtitleBoxTop = function () {
    var val = title['top'] + title['scaleY']*title['height'];
    subtitle_box.set({
        top: val
    });
};

var setSubtitleBoxHeight = function () {
    var titlevskip = emToPx(innertheme['title page']['title']['vskip']);
    var sep        = ptToPx(innertheme['title page']['subtitle']['sep']);

    var val        = titlevskip + subtitle_text['height'] + sep;

    subtitle_box.set({
        height: val
    });
};

// Subtitle text functions
var setSubtitleTextTop = function () {
    var factor      = scalefactor; //title['scaleY'];
    var titlevskip  = factor*emToPx(innertheme['title page']['title']['vskip']);
    var val         = subtitle_box['top'] + titlevskip;

    subtitle_text.set({
        top:       val
    });
};
// Subtitle box
var subtitle_box = innerBox('subtitle box');
subtitle_box.set({
    fill:   getColor('title', 'bg', colortheme['title']['bg']),
});

// Subtitle text
var subtitle_text = innerText('subtitle text', innertheme['title page']['subtitle']['text']);
subtitle_text.set({
    fill:       getColor('subtitle', 'fg', colortheme['subtitle']['fg']),
    fontSize:	getFontSize('subtitle'),
    fontFamily: getFontFamily('subtitle')
});

setSubtitleBoxTop();
setSubtitleBoxHeight(true);
setSubtitleTextTop('title page', 'subtitle');
initialiseInnerTextAlignment('title page', 'subtitle');

// Subtitle
var subtitle = immutableGroup('subtitle', [subtitle_text, subtitle_box], titlepagenum);
addInnerTemplate(subtitle, titlepagenum);

/*
* ------------------------------------------------------------------------------------------------
*      Author
* ------------------------------------------------------------------------------------------------
*/
// Author box
var author_box = innerBox('author box');
author_box.set({
    fill:   getColor('author', 'bg', colortheme['author']['bg']),
});

// Author text
var author_text = innerText('author text', innertheme['title page']['author']['text']);
author_text.set({
    fill:       getColor('author', 'fg', colortheme['author']['fg']),
    fontSize:	getFontSize('author'),
    fontFamily: getFontFamily('author')
});

setInnerBoxHeight('title page', 'author');
setInnerTextTop('title page', 'author');
initialiseInnerTextAlignment('title page', 'author');

// Author
var author = immutableGroup('author', [author_text, author_box], titlepagenum);
addInnerTemplate(author, titlepagenum);

/*
* ------------------------------------------------------------------------------------------------
*      Institute
* ------------------------------------------------------------------------------------------------
*/
// Institute box
var institute_box = innerBox('institute box');
institute_box.set({
    fill:   getColor('institute', 'bg', colortheme['institute']['bg']),
});

// Institute text
var institute_text = innerText('institute text', innertheme['title page']['institute']['text']);
institute_text.set({
    fill:       getColor('institute', 'fg', colortheme['institute']['fg']),
    fontSize:	getFontSize('institute'),
    fontFamily: getFontFamily('institute')
});

setInnerBoxHeight('title page', 'institute');
setInnerTextTop('title page', 'institute');
initialiseInnerTextAlignment('title page', 'institute');

// Institute
var institute = immutableGroup('institute', [institute_text, institute_box], titlepagenum);
addInnerTemplate(institute, titlepagenum);

/*
* ------------------------------------------------------------------------------------------------
*      Date
* ------------------------------------------------------------------------------------------------
*/
// Date box
var date_box = innerBox('date box');
date_box.set({
    fill:   getColor('date', 'bg', colortheme['date']['bg']),
});

// Date text
var date_text = innerText('date text', innertheme['title page']['date']['text']);
date_text.set({
    fill:       getColor('date', 'fg', colortheme['date']['fg']),
    fontSize:	getFontSize('date'),
    fontFamily: getFontFamily('date')
});

setInnerBoxHeight('title page', 'date');
setInnerTextTop('title page', 'date');
initialiseInnerTextAlignment('title page', 'date');

// Date
var date = immutableGroup('date', [date_text, date_box], titlepagenum);
addInnerTemplate(date, titlepagenum);


setTitlePageInnerVerticalLayout();
