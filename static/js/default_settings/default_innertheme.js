var getDate = function() {
    var today = new Date();

    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    var day = today.getDate();
    var month = months[today.getMonth()];
    var year = today.getFullYear();

    return month + ' ' + day + ', ' + year;
};


default_innertheme = {
    'titlepagecanvasgraphic': false,
    'toccanvasgraphic': false,
    'backgroundcanvasgraphic': false,

    'titlepagegraphic': {
        'set':      false,
        'width':    0,
        'height':   0,
        'angle':    0,
        'top':      0,
        'left':     0
    },

    'tocgraphic': {
        'set':      false,
        'width':    0,
        'height':   0,
        'angle':    0,
        'top':      0,
        'left':     0
    },

    'backgroundgraphic': {
        'set':      false,
        'width':    0,
        'height':   0,
        'angle':    0,
        'top':      0,
        'left':     0
    },

    'title page': {
        'title': {
            'text':         'Title: Beamer Slide Designer',
            'alignment':    'center',   //left, center, right
            'sep':          8,          //8pt
            'vsep':         'sep',
            'hsep':         'sep',
            'rounded':      'false',
            'shadow':       'false',   //If true, then it only takes affect if 'rounded' is selected
            'hspace*':      0,         //mm
            'vskip':        0.25       //em


            // 'ignorebg':  '<NOT SUPPORTED>'
            // 'vmode':     '<NOT SUPPORTED>'
            // 'wd':        '<NOT SUPPORTED>',
            // 'ht':        '<NOT SUPPORTED>',
            // 'leftskip':  '<NOT SUPPORTED>',  // Note: Conflicts with alignment setting
            // 'rightskip': '<NOT SUPPORTED>',
            // 'colsep':    '<NOT SUPPORTED>',
            // 'colsep*':   '<NOT SUPPORTED>',
        },

        'subtitle': {
            'text':         'Subtitle: A Graphic Theme Creation Tool',
            'alignment':    'center',
            'vsep':         'sep',
            'hsep':         'sep',
            'sep':          8,  //8pt
            'rounded':      'false',      //Not manipulatable but conforms to innertheme model
            'shadow':       'false',      //Not manipulatable but conforms to innertheme model
            'hspace*':      0,  //mm
            'vskip':        1,  //em
        },

        'author': {
            'text':         'Authors: Ryan Venter & Willem Bester',
            'alignment':    'center',   //left, center, right
            'vsep':         'sep',
            'hsep':         'sep',
            'sep':          8,          //8pt
            'rounded':      'false',
            'shadow':       'false',    //If true, then it only takes affect if 'rounded' is selected
            'hspace*':      0,          //mm
            'vskip':        0,          //em

        },

        'institute': {
            'text':         'Institute: Stellenbosch University',
            'alignment':    'center',   //left, center, right
            'vsep':         'sep',
            'hsep':         'sep',
            'sep':          8,          //8pt
            'rounded':      'false',
            'shadow':       'false',    //If true, then it only takes affect if 'rounded' is selected
            'hspace*':      0,          //mm
            'vskip':        0,          //em
        },

        'date':  {
            'text':         'Date: ' + getDate(),
            'alignment':    'center',   //left, center, right
            'vsep':         'sep',
            'hsep':         'sep',
            'sep':          8,          //8pt
            'rounded':      'false',
            'shadow':       'false',    //If true, then it only takes affect if 'rounded' is selected
            'hspace*':      0,          //mm
            'vskip':        0.5,        //em
        }
    },

    'toc': {
        'section in toc': {
            'text':         'Section Title',
            'option':       'default',
            'vsep':         'empty',
            'hsep':         'empty',
            'alignment':    'left',
            'rounded':      'false',      //Not manipulatable but conforms to innertheme model
            'shadow':       'false',      //Not manipulatable but conforms to innertheme model
            'hspace*':      0,
        },

        'subsection in toc': {
            'text':         'Subsection Title',
            'option':       'default',
            'vsep':         'empty',
            'hsep':         'leftskip',
            'alignment':    'left',
            'rounded':      'false',      //Not manipulatable but conforms to innertheme model
            'shadow':       'false',      //Not manipulatable but conforms to innertheme model
            'hspace*':      0,
            'leftskip':     1.5,    //measured in em
        }
    },

    'types of text': {
        'normal text': {
            'text':         'The most important thing in a programming language is the name. A language will not succeed without a good name. I have recently invented a very good name and now I am looking for a suitable language.',
            'vsep':         'empty',
            'hsep':         'empty',
            'alignment':    'left',
            'rounded':      'false',      //Not manipulatable but conforms to innertheme model
            'shadow':       'false',      //Not manipulatable but conforms to innertheme model
            'hspace*':      0,
        },

        'alerted text': {
            'text':         '...most important... ...programming language... ...the name... ...not succeed... ...invented... ...very good name... ...suitable language...',
            'vsep':         'empty',
            'hsep':         'empty',
            'alignment':    'left',
            'rounded':      'false',      //Not manipulatable but conforms to innertheme model
            'shadow':       'false',      //Not manipulatable but conforms to innertheme model
            'hspace*':      0,
        }
    },

    'types of blocks': {
        'block title': {
            'text':         'Normal block',
            'vsep':         'empty',
            'hsep':         'colsep*',
            'colsep*':      0.75,       //measured in em
            'alignment':    'left',
            'rounded':      'false',
            'shadow':       'false',    //If true, then it only takes affect if 'rounded' is selected
            'hspace*':      0,
        },

        'block body': {
            'text':         'A programmer got stuck in the shower because the instructions on the shampoo bottle said, "Lather, Rinse, Repeat".',
            'vsep':         'empty',
            'hsep':         'colsep*',
            'colsep*':      0.75,       //measured in em
            'alignment':    'left',
            'rounded':      'false',      //Not manipulatable but conforms to innertheme model
            'shadow':       'false',      //Not manipulatable but conforms to innertheme model
            'hspace*':      0,
        },

        'block title alerted': {
            'text':         'Alerted block',
            'vsep':         'empty',
            'hsep':         'colsep*',
            'colsep*':      0.75,           //measured in em
            'alignment':    'left',
            'rounded':      'false',
            'shadow':       'false',      //If true, then it only takes affect if 'rounded' is selected
            'hspace*':      0,
        },

        'block body alerted': {
            'text':         'Why do programmers always mix up Halloween and Christmas? Because Oct 31 == Dec 25',
            'vsep':         'empty',
            'hsep':         'colsep*',
            'colsep*':      0.75,       //measured in em
            'alignment':    'left',
            'rounded':      'false',    //Not manipulatable but conforms to innertheme model
            'shadow':       'false',    //Not manipulatable but conforms to innertheme model
            'hspace*':      0,
        },

        'block title example': {
            'text':         'Example block',
            'vsep':         'empty',
            'hsep':         'colsep*',
            'colsep*':      0.75,           //measured in em
            'alignment':    'left',
            'rounded':      'false',
            'shadow':       'false',      //If true, then it only takes affect if 'rounded' is selected
            'hspace*':      0,
        },

        'block body example': {
            'text':         'If you do not understand recurrence, read this sentence again.',
            'vsep':         'empty',
            'hsep':         'colsep*',
            'colsep*':      0.75,           //measured in em
            'alignment':    'left',
            'rounded':      'false',      //Not manipulatable but conforms to innertheme model
            'shadow':       'false',      //Not manipulatable but conforms to innertheme model
            'hspace*':      0,
        }
    }
};
