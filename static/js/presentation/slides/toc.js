/*
* =================================================================================================
*      Table of contents (toc)
* =================================================================================================
*/
tocnum = addSlide('toc');

var setTocInnerVerticalLayout = function () {
    var entry           = innertheme['toc'];

    var gap             = scalefactor*10;
    var secheight       = scalefactor*section_in_toc['height'];
    var subsecheight    = scalefactor*subsection_in_toc['height'];

    var boxheights      = 3*secheight + 6*subsecheight;
    var gaps            = 2*gap;

    var topmargin   = (scalefactor*textheight() - (boxheights + gaps))/2;

    var currtop = topmargin;
    for (var i in sectionsintoc) {
        sectionsintoc[i].set({
            top:    currtop
        });
        currtop += secheight;
        for (var j in subsectionsintoc[i]) {
            subsectionsintoc[i][j].set({
                top:    currtop
            });
            currtop += subsecheight;
        }
        currtop += gap;
    }
    slides[tocnum].renderAll();
};


var setTocItem = function (template, option) {
        // var template    = element + ' in toc';
        var name        = template + ' item';

        var texttop     = innerelements[template + ' text']['top'];
        var fontsize    = getFontSize(template);
        var itemsize    = fontsize/3;
        var vsep        = (fontsize-itemsize)/2;


        var item;
        switch(option) {
            case 'default':
                item = new fabric.Rect({
                    width:      0,
                    height:     0
                });
                break;
            case 'numbered':
                item = new fabric.Text('X.X', {
                    top:        texttop,
                    fontSize:   fontsize,
                });
                break;
            case 'circle':
                item = new fabric.Circle({
                    radius:     itemsize/2
                });
                break;
            case 'square':
                item = new fabric.Rect({
                    width:      itemsize,
                    height:     itemsize
                });
                break;
            // case 'ball':
            //     item = new fabric.Circle();
            //     break;
            // case 'ball unnumbered':
            //     break;
            // case 'image': //coming soon
            //     break;
            default:
                console.log('TOC ITEM OPTION ERROR - ', option);
                break;
        }

        item.set({
            'name':     name,
            'option':   option,
            originY:    'center',
            top:        texttop + itemsize + vsep,
            fill:       getColor(template, 'fg', colortheme[template]['fg']),
        });

        innerelements[name] = item;
        return item;
};

var setTocTextAlignment = function (name) {
    var entry       = innertheme['toc'][name];
    var alignment   = entry['alignment'];
    var hspace      = mmToPx(entry['hspace*']);
    var sep;
    if ((opt = entry['hsep']) == 'empty') {
        sep = 5;
    } else {
        sep = emToPx(entry[opt]);               //NOTE EM-TO-PX NOT PT
    }

    var text = innerelements[name + ' text'];
    var item = innerelements[name + ' item'];
    var val = 0;
    var itemsep = 0;
    switch (alignment) {
        case 'left':
            val = leftspace() + sep + hspace;
            break;
        case 'center':
            val = leftspace() + (textwidth()/2) + hspace;
            break;
        case 'right':
            val = paperwidth() - rightspace() - sep;

            break;
        default:
            console.log("ALIGNMENT PROPERTY OF", template, ' - ', name, ' NOT SET');
            break;
    }

    item.set({
        originX:    alignment,
        left:       val
    });

    if (item['type'] != 'text') {
        itemsep = 2;
    }
    text.set({
        originX:    alignment,
        left:       val + item['width'] + itemsep
    });
};
/* ---- Refresh slide fonts --------------------------------------------------------------------- */
var refreshTocFonts = function (name) {
    updateSectionInTocFont();
    updateSubsectionInTocFont();

    setTocInnerVerticalLayout();
    slides[tocnum].renderAll();
    if (name != 'empty') {
        slides[tocnum].setActiveObject(innertemplates[name]);
    }
    slides[tocnum].renderAll();
};

/* ---- Refresh slide colors -------------------------------------------------------------------- */
var refreshTocColors = function () {
    innerelements['section in toc text'].set({
        fill:   getColor('section in toc', 'fg', colortheme['section in toc']['fg'])
    });

    innerelements['section in toc box'].set({
        fill:   getColor('normal text', 'bg', colortheme['normal text']['bg'])
    });

    innerelements['subsection in toc text'].set({
        fill:   getColor('subsection in toc', 'fg', colortheme['subsection in toc']['fg'])
    });

    innerelements['subsection in toc box'].set({
        fill:   getColor('normal text', 'bg', colortheme['normal text']['bg'])
    });

    slides[tocnum].renderAll();
};

/* ---- Refresh slide shadows ------------------------------------------------------------------- */
// var refreshTocShadows = function () {
//     var templates = ['title', 'subtitle', 'author', 'institute', 'date'];
//     for (var i in templates) {
//         var name = templates[i]
//         setTemplateShadow(templates[i], innertheme['title page'][templates[i]]['rounded'],innertheme['title page'][templates[i]]['shadow']);
//     }
//     setTemplateShadow('subtitle', innertheme['title page']['title'['rounded'],innertheme['title page']['title']['shaodw'])
//     slides[toc].renderAll();
// };

/* ---- Attach update function to slide -------------------------------------------------------- */
slides[tocnum].refreshFontsFunction = function (name) { refreshTocFonts(name); };
slides[tocnum].refreshColorsFunction = function () { refreshTocColors(); };
slides[tocnum].refreshShadowsFunction = function () {}; //shadow effects cannot be set for toc




/*
* ------------------------------------------------------------------------------------------------
*      Section in toc
* ------------------------------------------------------------------------------------------------
*/

var sectionsintoc;  //Array of section object groups

//---- Section in toc functions ------------------------------------------------------------------

var updateSectionInTocFont = function () {
    var template    = innertemplates['section in toc'];
    var text        = innerelements['section in toc text'];
    var box         = innerelements['section in toc box'];
    var alignment   = innertheme['toc']['section in toc']['alignment'];


    text.set({
        fontSize:   getFontSize('section in toc'),
        fontFamily: getFontFamily('section in toc')
    });
    wrapText(text, tocnum);
    box.set({
        left:   leftspace(),
        width:  textwidth()
    });

    setInnerBoxHeight('toc','section in toc');
    setTocTextAlignment('section in toc');


    slides[tocnum].remove(section_in_toc);
    section_in_toc = immutableGroup('section in toc', [text, box], tocnum);
    if (section_in_toc_item['option'] == 'numbered') {
        section_in_toc_item.set({
            fontFamily:  getFontFamily('section in toc')
        });
    }
    removeSectionsInToc();
    cloneAndAddSectionsInToc();
    // addInnerTemplate(section_in_toc, tocnum);
    // setInnerTextAlignment('toc', 'section in toc', alignment, tocnum);

};

var setTitleTextAlignment = function (alignment) {
        setInnerTextAlignment('title page', 'title', alignment, titlepagenum);
        setInnerTextAlignment('title page', 'subtitle', alignment, titlepagenum);
};

var refreshSectionsintoc = function () {
    sectionsintoc = [innertemplates['section in toc'], innertemplates['section in toc 2'], innertemplates['section in toc 3']];
};

var removeSectionsInToc = function () {
    for (var i in sectionsintoc) {
        slides[tocnum].remove(sectionsintoc[i]);
    }
};
var setSectionInTocItem = function (option) {
    removeSectionsInToc();

    section_in_toc_item = setTocItem('section in toc', option);
    setTocTextAlignment('section in toc');
    section_in_toc_box.set({
        left:   leftspace()
    });
    section_in_toc  = immutableGroup('section in toc', [section_in_toc_item, section_in_toc_text, section_in_toc_box]);

    cloneAndAddSectionsInToc(option);
    setTocInnerVerticalLayout();

};

var cloneAndAddSectionsInToc = function(option ='default') {
    for (var i = 2; i < 4; i++) {
        var temp = immutableGroup('section in toc ' + i, [fabric.util.object.clone(section_in_toc_item), section_in_toc_text, section_in_toc_box], tocnum, 'section in toc');
        temp.set({
            originX:    'left',
            left:       leftspace(),
        });
        addInnerTemplate(temp, tocnum);
    }

    addInnerTemplate(section_in_toc, tocnum);

    refreshSectionsintoc();

    if (option == 'numbered') {
        for (var i in sectionsintoc) {
            sectionsintoc[i].item(0).setText((parseInt(i)+1)+ '.');
        }
    }
};

//---- Section in toc variables ------------------------------------------------------------------

// Section in toc box
var section_in_toc_box = innerBox('section in toc box');
section_in_toc_box.set({
    fill:   getColor('normal text', 'bg', colortheme['normal text']['bg']),
});

// Section in toc text
var section_in_toc_text = innerText('section in toc text', innertheme['toc']['section in toc']['text']);
section_in_toc_text.set({
    fill:       getColor('section in toc', 'fg', colortheme['section in toc']['fg']),
    fontSize:	getFontSize('section in toc'),
    fontFamily: getFontFamily('section in toc')
});

setInnerBoxHeight('toc', 'section in toc');
setInnerTextTop('toc', 'section in toc');

// Section in toc item
var section_in_toc_item = setTocItem('section in toc', 'default');
setTocTextAlignment('section in toc');
//
// // Section in toc
var section_in_toc = immutableGroup('section in toc', [section_in_toc_item, section_in_toc_text, section_in_toc_box], tocnum);


cloneAndAddSectionsInToc();


/*
* ------------------------------------------------------------------------------------------------
*      Subsection in toc
* ------------------------------------------------------------------------------------------------
*/

var subsectionsintoc;  //Array of section object groups

//---- Subsection in toc functions ------------------------------------------------------------------
var updateSubsectionInTocFont = function () {
    var template    = innertemplates['subsection in toc'];
    var text        = innerelements['subsection in toc text'];
    var box         = innerelements['subsection in toc box'];
    var alignment   = innertheme['toc']['subsection in toc']['alignment'];


    text.set({
        fontSize:   getFontSize('subsection in toc'),
        fontFamily: getFontFamily('subsection in toc')
    });
    wrapText(text, tocnum);
    box.set({
        left:   leftspace(),
        width:  textwidth()
    });

    setInnerBoxHeight('toc','subsection in toc');
    setTocTextAlignment('subsection in toc');


    slides[tocnum].remove(subsection_in_toc);
    subsection_in_toc = immutableGroup('subsection in toc', [text, box], tocnum);
    if (subsection_in_toc_item['option'] == 'numbered') {
        subsection_in_toc_item.set({
            fontFamily:  getFontFamily('subsection in toc')
        });
    }
    removeSubsectionsInToc();
    cloneAndAddSubsectionsInToc();
    // addInnerTemplate(section_in_toc, tocnum);
    // setInnerTextAlignment('toc', 'section in toc', alignment, tocnum);

};
var refreshSubsectionsintoc = function () {
    subsectionsintoc = [
        [innertemplates['subsection in toc'], innertemplates['subsection in toc 1.2']],
        [innertemplates['subsection in toc 2.1'], innertemplates['subsection in toc 2.2']],
        [innertemplates['subsection in toc 3.1'], innertemplates['subsection in toc 3.2']],
    ];
};

var removeSubsectionsInToc = function () {
    for (var i = 0; i < sectionsintoc.length; i++) {
        for (var j in subsectionsintoc[i]) {
            slides[tocnum].remove(subsectionsintoc[i][j]);
        }
    }
};

var cloneAndAddSubsectionsInToc = function(option ='default') {

    // Subsection 1.2
    var temp = immutableGroup('subsection in toc 1.2', [fabric.util.object.clone(subsection_in_toc_item), subsection_in_toc_text, subsection_in_toc_box], tocnum, 'subsection in toc');
    temp.set({
        originX:    'left',
        left:       leftspace(),
        'option':   option
    });
    addInnerTemplate(temp, tocnum);

    // The remaining subsections
    for (var i = 2; i <= sectionsintoc.length; i++) {
        for (var j = 1; j < 3; j++) {
            var temp = immutableGroup('subsection in toc ' + i + '.' + j, [fabric.util.object.clone(subsection_in_toc_item), subsection_in_toc_text, subsection_in_toc_box], tocnum, 'subsection in toc');
            temp.set({
                originX:    'left',
                left:       leftspace()
            });
            addInnerTemplate(temp, tocnum);
        }
    }

    addInnerTemplate(subsection_in_toc, tocnum);

    refreshSubsectionsintoc();

    if (option == 'numbered') {
        for (var i = 1; i <=  sectionsintoc.length; i++) {
            for (var j = 1; j <=  subsectionsintoc[i-1].length; j++) {
                subsectionsintoc[i-1][j-1].item(0).setText(i + '.' + j);
            }
        }
    }
};

var setSubsectionInTocItem = function (option) {
    removeSubsectionsInToc();

    subsection_in_toc_item = setTocItem('subsection in toc', option);
    setTocTextAlignment('subsection in toc');
    subsection_in_toc_box.set({
        left:   leftspace()
    });

    subsection_in_toc  = immutableGroup('subsection in toc', [subsection_in_toc_item, subsection_in_toc_text, subsection_in_toc_box], tocnum);

    cloneAndAddSubsectionsInToc(option);
    setTocInnerVerticalLayout();
};
//
// var updateSubsectionInToc = function (option, secnum='1', tocnum=1) {
//     slides[tocnum].remove(innertemplates['subsection in toc']);
//     // Subsection in toc box
//     var subsection_in_toc_box = innerBox('subsection in toc box');
//     subsection_in_toc_box.set({
//         fill:   'rgba(0,0,0,0)' //getColor('subsection in toc', 'bg', colortheme['subsection in toc']['bg']),
//     });
//
//     // Subsection in toc text
//     var subsection_in_toc_text = innerText('subsection in toc text', innertheme['toc']['subsection in toc']['text']);
//     subsection_in_toc_text.set({
//         fill:       getColor('subsection in toc', 'fg', colortheme['subsection in toc']['fg']),
//         fontSize:	getFontSize('subsection in toc'),
//     });
//
//     setInnerBoxHeight('toc', 'subsection in toc');
//     setInnerTextTop('toc', 'subsection in toc');
//
//     // Subsection in toc item
//     var subsection_in_toc_item = setTocItem('subsection in toc', option);
//     setTocTextAlignment('subsection in toc');
//     //
//     // // Subsection in toc
//     var subsection_in_toc = immutableGroup('subsection in toc', [subsection_in_toc_item, subsection_in_toc_text, subsection_in_toc_box]);
//
//     addInnerTemplate(subsection_in_toc, tocnum);
// };

// Subsection in toc box
var subsection_in_toc_box = innerBox('subsection in toc box');
subsection_in_toc_box.set({
    fill:   getColor('normal text', 'bg', colortheme['normal text']['bg']),
});

// Subsection in toc text
var subsection_in_toc_text = innerText('subsection in toc text', innertheme['toc']['subsection in toc']['text']);
subsection_in_toc_text.set({
    fill:       getColor('subsection in toc', 'fg', colortheme['subsection in toc']['fg']),
    fontSize:	getFontSize('subsection in toc'),
    fontFamily: getFontFamily('subsection in toc')
});

setInnerBoxHeight('toc', 'subsection in toc');
setInnerTextTop('toc', 'subsection in toc');

// Subsection in toc item
var subsection_in_toc_item = setTocItem('subsection in toc', 'default');
setTocTextAlignment('subsection in toc');

// // Subsection in toc
var subsection_in_toc = immutableGroup('subsection in toc', [subsection_in_toc_item, subsection_in_toc_text, subsection_in_toc_box], tocnum);

cloneAndAddSubsectionsInToc();

setTocInnerVerticalLayout();



// var subsection_in_toc12 = fabric.util.object.clone(subsection_in_toc);
// subsection_in_toc12.set({
//     name: 'subsection in toc 12'
// });
// var subsection_in_toc21 = fabric.util.object.clone(subsection_in_toc);
// subsection_in_toc21.set({
//     name: 'subsection in toc 21'
// });
// var subsection_in_toc22 = fabric.util.object.clone(subsection_in_toc);
// subsection_in_toc22.set({
//     name: 'subsection in toc 22'
// });
// var subsection_in_toc31 = fabric.util.object.clone(subsection_in_toc);
// subsection_in_toc31.set({
//     name: 'subsection in toc 31'
// });
// var subsection_in_toc32 = fabric.util.object.clone(subsection_in_toc);
// subsection_in_toc32.set({
//     name: 'subsection in toc 32'
// });
//
// addInnerTemplate(subsection_in_toc, tocnum);
// addInnerTemplate(subsection_in_toc12, tocnum);
//
// addInnerTemplate(subsection_in_toc21, tocnum);
// addInnerTemplate(subsection_in_toc22, tocnum);
//
//
// addInnerTemplate(subsection_in_toc31, tocnum);
// addInnerTemplate(subsection_in_toc32, tocnum);
//
// subsectionsintoc = [
//     [innertemplates['subsection in toc'], innertemplates['subsection in toc 12']],
//     [innertemplates['subsection in toc 21'], innertemplates['subsection in toc 22']],
//     [innertemplates['subsection in toc 31'], innertemplates['subsection in toc 32']],
// ];
