var rgbColor = function (color) {
    var start = color.indexOf('(');
    var end = color.indexOf(')');
    var rgb = color.substring(start+1,end).split(',');
    return rgb;
}

var defColor = function (color) {
    return "rgb("+color[0]+","+color[1]+","+color[2]+")";
}

var mixColors = function (color, name, attr, target) {
    var rgb = rgbColor(color);
    var strength = target['mix']['strength']/100;
    var mix = rgbColor(getColor(name, attr, target['mix']));

    for (i in rgb) {
        rgb[i] *= strength;
        mix[i] *= (1-strength);
        rgb[i] += mix[i];
        rgb[i] = parseInt(rgb[i]);
    }
    return defColor(rgb);
}

var getColor = function (name, attr, target) {
    var type = target['type'];
    var val = type.split(' ');

    var color;
    switch (val[0]) {
        case 'color':
            color =  target['value'];
            break;
        case 'inherit':
            var tempattr = attr;
            if (type.search('attr') >= 0) {
                tempattr = target['attr']
            }
            color = getColor(target['use'], tempattr, colortheme[target['use']][tempattr]);
            break;
        case 'attr':
            if (target['attr'] == attr) {
                if (attr == 'fg') {
                    color = getColor(fg_empty, attr, colortheme[fg_empty][attr]);
                } else {
                    color = getColor(bg_empty, attr, colortheme[bg_empty][attr]);
                }
            } else {
                color = getColor(name, target['attr'], colortheme[name][target['attr']]);
            }
            break;
        case 'empty':
            if (attr == 'fg') {
                return getColor(fg_empty, attr, colortheme[fg_empty][attr]);
            } else {
                return getColor(bg_empty, attr, colortheme[bg_empty][attr]);
            }
            break;
        default:
            console.log('UNRECOGNIZED SIMPLE COLOR DEFINITION -', val[0]);
    }

    if (type.search('mix') >= 0) {
        return mixColors(color, name, attr, target);
    } else {
        return color;
    }
};
