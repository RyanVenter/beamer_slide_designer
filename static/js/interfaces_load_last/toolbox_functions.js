/*
 * ================================================================================================
 *     Color tool functions
 * ================================================================================================
*/

/* ------------------------------------------------------------------------------------------------
 * === Create jscolor elements ====================================================================
 * ---------------------------------------------------------------------------------------------- */
 var updateFg = function(elem) {

     if ((target = getActiveTemplate()) != "") {
         var slidenum   = getActiveSlide();
         var target     = getActiveTemplate();

         var red         = Math.round(elem.rgb[0]);
         var green       = Math.round(elem.rgb[1]);
         var blue        = Math.round(elem.rgb[2]);

         var value       = 'rgb(' + red + ',' + green + ',' + blue + ')';
        colortheme[target]['fg'] = {
                'type':     'color',
                'value':    value
            }

        refreshSlideColors();
    }
 };

 var updateBg = function(elem) {
     if ((target = getActiveTemplate()) != "") {
         var slidenum    = getActiveSlide();


         var red         = Math.round(elem.rgb[0]);
         var green       = Math.round(elem.rgb[1]);
         var blue        = Math.round(elem.rgb[2]);

         var value       = 'rgb(' + red + ',' + green + ',' + blue + ')';
        colortheme[target]['bg'] = {
                'type':     'color',
                'value':    value
            }
        refreshSlideColors();
    }
 };

 // $('.default-color-fill').on('mousedown', function() {
 //     var opt         = $(this).attr('target');
 //
 //     var target      = getActiveTemplate();
 //     var slidenum    = getActiveSlide();
 //     var name        = slides[slidenum].getActiveObject()['name'];
 //     var slidenum    = innertemplates[target]['slidenum'];
 //
 //     colortheme[target][opt] =
 // })

/* ------------------------------------------------------------------------------------------------
 * === ADDITIONAL FUNCTIONS =======================================================================
 * ---------------------------------------------------------------------------------------------- */
 var toggleIcon = function(elem, remove_icon, add_icon) {
     elem.removeClass(remove_icon);
     elem.addClass(add_icon);
 };

/* ------------------------------------------------------------------------------------------------
 * === Create salt-pepper-cumin tools =============================================================
 * ---------------------------------------------------------------------------------------------- */

/* --- SALT ------------------------------------------------------------------------------------- */
var pepper_states = {}
var cumin_states = {}
var togglePepper = function(salt, num) {
    var pepper =  $('[class*="pepper"][color-tool-num="'+num+'"]');
    var cumin = $('[class*="cumin"][color-tool-num="'+num+'"][cumin-reference="'+cumin_states[num]+'"]');
    var closed_icon = 'glyphicon-triangle-right';
    var open_icon = 'glyphicon-triangle-bottom';
    var icon = $(salt.find('[class^="tool-icon"]'));

    if (pepper_states[num]) {
        cumin.addClass('hidden');

        pepper_states[num] = !pepper_states[num];
        pepper.addClass('hidden');

        toggleIcon(icon, open_icon, closed_icon);
    } else {
        toggleIcon(icon, closed_icon, open_icon)

        pepper_states[num] = !pepper_states[num];
        pepper.removeClass('hidden');

        cumin.removeClass('hidden');
    }
};

var saltElem = function(salt) {
        var color_tool_num = salt.attr('color-tool-num');
        pepper_states[color_tool_num] = false;
        salt.on('mousedown', function() {
            togglePepper(salt, color_tool_num);
        });
};

$('.salt').each(function() {
    new saltElem($(this));
});

/* --- PEPPER ----------------------------------------------------------------------------------- */

var setActiveCuminTool = function(cumin, num, reference) {
    cumin.addClass('hidden');
    var target = $(cumin.filter('[cumin-reference="'+reference+'"]'));
    target.removeClass('hidden');
    cumin_states[num]=reference;
}

var pepperItemElem = function(pepper_item, num) {
    var cumin_reference = pepper_item.attr('data-value');
    var cumin = $('.cumin[color-tool-num="'+num+'"]');
    pepper_item.on('mousedown', function() {
        setActiveCuminTool(cumin, num, cumin_reference)
    });

}

var pepperElem = function(pepper) {
    var color_tool_num = pepper.attr('color-tool-num');
    cumin_states[color_tool_num]="";
    pepper.find('.item').each(function () {
        new pepperItemElem($(this), color_tool_num);
    })
};

$('.pepper').each(function() {
    new pepperElem($(this));
});

/*
 * ================================================================================================
 *     Font tool functions
 * ================================================================================================
*/

/* Font size selection */
$('#font_size_selection .item').on('mousedown', function() {
    // var opt         = $(this).attr('data-value');
    // var target      = getActiveTemplate();
    // var slidenum    = innertemplates[target]['slidenum'];
    var opt         = $(this).attr('data-value');
    var target      = getActiveTemplate();
    var slidenum    = getActiveSlide();
    var name        = slides[slidenum].getActiveObject()['name'];
    var slidenum    = innertemplates[target]['slidenum'];

    if (target != "") {
        if (opt == 'default') {
            opt = default_fonttheme[target]['size'];
        }
        fonttheme[target]['size'] = opt;

        refreshSlideFonts();
        slides[slidenum].setActiveObject(innertemplates[name]);
    }

});

/* Font series selection */
$('#font_series_selection .item').on('mousedown', function() {
    // var opt = $(this).attr('data-value');
    // var target = getActiveTemplate();
    // var slidenum    = innertemplates[target]['slidenum'];
    var opt         = $(this).attr('data-value');
    var target      = getActiveTemplate();
    var slidenum    = getActiveSlide();
    var name        = slides[slidenum].getActiveObject()['name'];
    var slidenum    = innertemplates[target]['slidenum'];


    if (target != "") {
        if (opt == 'default') {
            opt = default_fonttheme[target]['series'];
        }
        fonttheme[target]['series'] = opt;

        refreshSlideFonts();
        slides[slidenum].setActiveObject(innertemplates[name]);
    }
});


/* Font shape selection */
$('#font_shape_selection .item').on('mousedown', function() {
    var opt         = $(this).attr('data-value');
    var target      = getActiveTemplate();
    var slidenum    = getActiveSlide();
    var name        = slides[slidenum].getActiveObject()['name'];
    var slidenum    = innertemplates[target]['slidenum'];


    if (target != "") {
        if (opt == 'default') {
            opt = default_fonttheme[target]['shape'];
        }
        fonttheme[target]['shape'] = opt;


        refreshSlideFonts();
        slides[slidenum].setActiveObject(innertemplates[name]);
    }
});

/* Font family selection */
$('#font_family_selection .item').on('mousedown', function() {
    var opt         = $(this).attr('data-value');
    var target      = getActiveTemplate();
    var slidenum    = getActiveSlide();
    var name        = slides[slidenum].getActiveObject()['name'];
    var slidenum    = innertemplates[target]['slidenum'];


    if (target != "") {
        if (opt == 'default') {
            opt = default_fonttheme[target]['family'];
        }
        fonttheme[target]['family'] = opt;

        refreshSlideFonts();
        slides[slidenum].setActiveObject(innertemplates[name]);
    }
});

/*
 * ================================================================================================
 *     Appearance tool functions
 * ================================================================================================
*/

/* alignment selection */
$('#alignment_selection .item').on('mousedown', function() {
    var alignment   = $(this).attr('data-value');
    var target      = getActiveTemplate();
    var slidenum    = getActiveSlide();
    var name        = slides[slidenum].getActiveObject()['name'];
    var template    = slides[slidenum]['name'];

    var slidenum    = innertemplates[target]['slidenum'];

    var restrictions = [
        'background canvas',
        'subtitle',
        'section in toc',
        'subsection in toc',
        'normal text',
        'alerted text'
    ]
    if (restrictions.indexOf(target) == -1) {
        if (alignment == 'default') {
            alignment = default_innertheme[template][target]['alignment'];
        }
        setInnerTextAlignment(template, target, alignment, slidenum);
        if (target == 'title')  {
            setInnerTextAlignment(template, 'subtitle', alignment, slidenum);

        }
        slides[slidenum].renderAll();
    }


});

/* rounded selection */
$('#rounded_selection .item').on('mousedown', function() {
    var rounded     = $(this).attr('data-value');
    var target      = getActiveTemplate();
    var slidenum    = getActiveSlide();
    // var name        = slides[slidenum].getActiveObject()['name'];
    var template    = slides[slidenum]['name'];

    var slidenum    = innertemplates[target]['slidenum'];

    var restrictions = ['background canvas',
                        'subtitle',
                        'section in toc',
                        'subsection in toc',
                        'subtitle',
                        'normal text',
                        'alerted text'
                    ];
    if (restrictions.indexOf(target) == -1) {
        if (rounded == 'default') {
            rounded = default_innertheme[template][target]['rounded'];
        }
        innertheme[template][target]['rounded'] = rounded;
        slides[slidenum].refreshShadowsFunction();
    }
});

/* shadow selection */
$('#shadow_selection .item').on('mousedown', function() {
    var shadow     = $(this).attr('data-value');
    var target      = getActiveTemplate();
    var slidenum    = getActiveSlide();
    // var name        = slides[slidenum].getActiveObject()['name'];
    var template    = slides[slidenum]['name'];

    var slidenum    = innertemplates[target]['slidenum'];

    var restrictions = ['background canvas',
                        'subtitle',
                        'section in toc',
                        'subsection in toc',
                        'subtitle',
                        'normal text',
                        'alerted text'
                    ];

    if (restrictions.indexOf(target) == -1) {
        if (shadow == 'default') {
            shadow = default_innertheme[template][target]['shadow'];
        }
        innertheme[template][target]['shadow'] = shadow;
        slides[slidenum].refreshShadowsFunction();
    }
});
