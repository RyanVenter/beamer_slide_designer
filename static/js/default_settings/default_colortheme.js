/* =================================================================================================
 * === Attribute explanations ======================================================================
 * ============================================================================================== */
/*  use:    color  -- means use the value specified in value
            empty   -- unset => handle by setting color to 'rgba(0,0,0,0)' in handler function
            <name>  -- inherit value from <name>
    strength:  mix value with percentage <strength> with BLACK (100-<strength>)%
            - not needed for true colors => true colors strength = 100
*/
var fg_empty = 'normal text';
var bg_empty = 'background canvas';


var default_colortheme = {
    'test color def': {
        fg: {
            type:   'color mix',
            value:  'rgb(255,255,100)',
            mix:    {

                strength:  40,
                type:   'color',
                value:   'rgb(33,33,33)',
            }
        },
        bg: {
            type:   'inherit mix',
            use:    'palette secondary',
            mix:    {
                strength:  40,
                type:   'color mix',
                value:  'rgb(255,33,33)',
            }
        }
    },

    'test lineage': {
        fg: {
            type:   'inherit attr mix',
            use:    'palette secondary',
            attr:   'bg',
            mix:    {
                strength:  40,
                type:  'inherit',
                use:   'test mix',
            }
        },
        bg: {
            type:   'inherit mix',
            use:    'palette secondary',
            mix:    {
                strength:  40,
                type:   'inherit',
                use:  'palette sidebar secondary',
            }
        }
    },

    'test mix': {
        fg: {
            type:   'inherit mix',
            use:    'palette secondary',
            mix:    {
                strength:  40,
                type:  'color',
                value:   'rgb(123,123,123)',
            }
        },
        bg: {
            type:   'inherit mix',
            use:    'palette secondary',
            mix:    {
                strength:  40,
                type:   'inherit',
                use:  'palette sidebar secondary',
            }
        }
    },

    'normal text': {
        fg: {
            type:  'color',
            value:  'rgb(0,0,0)'
        },
        bg: {
            type:  'color',
            value:  'rgb(255,255,255)'
        }
    },

    'alerted text': {
        fg: {
            type:  'color',
            value:  'rgb(255,0,0)'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'example text': {
        fg: {
            type:   'color mix',
            value:  'rgb(0,255,0)',
            mix:    {
                strength:   50,
                type:       'color',
                value:      'rgb(0,0,0)'
            }
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'structure': {
        fg: {
            type:   'color',
            value:  'rgb(51,51,179)'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'background canvas': {
        fg: {
            type:   'inherit',
            use:    'normal text'
        },
        bg: {
            type:   'inherit',
            use:    'normal text'
        }
    },

    'background': {
        fg: {
            type:   'inherit',
            use:    'background canvas',
        },
        bg: {
            type:   'inherit',
            use:    'background canvas',
        }
    },

    'palette primary': {
        fg: {
            type:   'inherit',
            use:    'structure',
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'palette secondary': {
        fg: {
            type:   'inherit mix',
            use:    'structure',
            mix:    {
                strength:  75,
                type:   'color',
                value:  'rgb(0,0,0)'
            }
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'palette tertiary': {
        fg: {
            type:   'inherit mix',
            use:    'structure',
            mix:    {
                strength:  50,
                type:   'color',
                value:  'rgb(0,0,0)'
            }
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'palette quaternary': {
        fg: {
            type:   'color',
            value:  'rgb(0,0,0)'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'palette sidebar primary': {
        fg: {
            type:  'inherit',
            use:    'normal text'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'palette sidebar secondary': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'palette sidebar tertiary': {
        fg: {
            type:   'inherit',
            use:    'normal text'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'palette sidebar quaternary': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'math text': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'math text inlined': {
        fg: {
            type:   'inherit',
            use:    'math text'
        },
        bg: {
            type:   'inherit',
            use:    'math text'
        }
    },

    'math text displayed': {
        fg: {
            type:   'inherit',
            use:    'math text'
        },
        bg: {
            type:   'inherit',
            use:    'math text'
        }
    },

    'normal text in math text':  {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'local structure': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'inherit',
            use:    'structure'
        }
    },

    'titlelike': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'inherit',
            use:    'structure'
        }
    },

    'title': {
        fg: {
            type:   'inherit',
            use:    'titlelike'
        },
        bg: {
            type:   'inherit',
            use:    'titlelike'
        }
    },

    'title in head/foot': {
        fg: {
            type:   'inherit',
            use:    'palette quaternary'
        },
        bg: {
            type:   'inherit',
            use:    'palette quaternary'
        }
    },

    'title in sidebar': {
        fg: {
            type:   'inherit',
            use:    'palette sidebar quaternary'
        },
        bg: {
            type:   'inherit',
            use:    'palette sidebar quaternary'
        }
    },

    'subtitle': {
        fg: {
            type:   'inherit',
            use:    'title'
        },
        bg: {
            type:   'inherit',
            use:    'title'
        }
    },

    'author': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },


    'author in head/foot': {
        fg: {
            type:   'inherit',
            use:    'palette primary'
        },
        bg: {
            type:   'inherit',
            use:    'palette primary'
        }
    },

    'author in sidebar': {
        fg: {
            type:   'inherit',
            use:    'palette sidebar tertiary'
        },
        bg: {
            type:   'empty',
            use:    'sidebar'
        }
    },

    'institute': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'institute in head/foot': {
        fg: {
            type:   'inherit',
            use:    'palette tertiary'
        },
        bg: {
            type:   'inherit',
            use:    'palette tertiary'
        }
    },

    'institute in sidebar': {
        fg: {
            type:   'inherit',
            use:    'palette sidebar tertiary'
        },
        bg: {
            type:   'empty',
            use:    'sidebar'
        }
    },

    'date': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'date in head/foot': {
        fg: {
            type:   'inherit',
            use:    'palette secondary'
        },
        bg: {
            type:   'inherit',
            use:    'palette secondary'
        }
    },

    'date in sidebar': {
        fg: {
            type:   'inherit',
            use:    'palette sidebar tertiary'
        },
        bg: {
            type:   'empty',
            use:    'sidebar'
        }
    },

    'titlegraphic': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'part name': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'part title': {
        fg: {
            type:   'inherit',
            use:    'titlelike'
        },
        bg: {
            type:   'inherit',
            use:    'titlelike'
        }
    },

    'section name':  {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'section title':  {
        fg: {
            type:   'inherit',
            use:    'titlelike'
        },
        bg: {
            type:   'inherit',
            use:    'titlelike'
        }
    },

    'section in toc': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'inherit',
            use:    'structure'
        }
    },

    'section in toc shaded': {
        fg: {
            type:   'inherit',
            use:    'section in toc'
        },
        bg: {
            type:   'inherit',
            use:    'section in toc'
        }
    },

    'section in head/foot': {
        fg: {
            type:   'inherit',
            use:    'palette tertiary'
        },
        bg: {
            type:   'inherit',
            use:    'palette tertiary'
        }
    },

    'section in sidebar': {
        fg: {
            type:   'inherit',
            use:    'palette sidebar secondary'
        },
        bg: {
            type:   'inherit',
            use:    'palette sidebar secondary'
        }
    },

    'section in sidebar shaded': {
        fg: {
            type:   'inherit mix',
            use:    'section in sidebar',
            mix:    {

                strength:  40,
                type:   'attr',
                attr:   'bg',
            }
        },
        bg: {
            type:   'empty',
            use:    'sidebar'
        }
    },

    'section number projected': {
        fg: {
            type:   'inherit',
            use:    'item projected'
        },
        bg: {
            type:   'inherit',
            use:    'item projected'
        }
    },

    'subsection name': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'subsection title': {
        fg: {
            type:   'inherit',
            use:    'titlelike'
        },
        bg: {
            type:   'inherit',
            use:    'titlelike'
        }
    },

    'subsection in toc': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'subsection in toc shaded': {
        fg: {
            type:   'inherit',
            use:    'subsection in toc'
        },
        bg: {
            type:   'inherit',
            use:    'subsection in toc'
        }
    },

    'subsection in head/foot': {
        fg: {
            type:   'inherit',
            use:    'palette secondary'
        },
        bg: {
            type:   'inherit',
            use:    'palette secondary'
        }
    },

    'subsection in sidebar': {
        fg: {
            type:   'inherit',
            use:    'palette sidebar primary'
        },
        bg: {
            type:   'inherit',
            use:    'palette sidebar primary'
        }
    },

    'subsection in sidebar shaded': {
        fg: {
            type:   'inherit mix',
            use:    'subsection in sidebar',
            mix:    {
                strength:  40,
                type:   'attr',
                attr:   'bg'
            }
        },
        bg: {
            type:   'empty',
            use:    'sidebar'
        }
    },

    'subsection number projected': {
        fg: {
            type:   'inherit',
            use:    'subitem projected'
        },
        bg: {
            type:   'inherit',
            use:    'subitem projected'
        }
    },

    'subsubsection in toc': {
        fg: {
            type:   'inherit',
            use:    'subsection in toc'
        },
        bg: {
            type:   'inherit',
            use:    'subsection in toc'
        }
    },

    'subsubsection in toc shaded': {
        fg: {
            type:   'inherit',
            use:    'subsubsection in toc'
        },
        bg: {
            type:   'inherit',
            use:    'subsubsection in toc'
        }
    },

    'subsubsection in head/foot': {
        fg: {
            type:   'inherit',
            use:    'subsection in head/foot'
        },
        bg: {
            type:   'inherit',
            use:    'subsection in head/foot'
        }
    },

    'subsubsection in sidebar': {
        fg: {
            type:   'inherit',
            use:    'subsection in sidebar'
        },
        bg: {
            type:   'inherit',
            use:    'subsection in sidebar'
        }
    },

    'subsubsection in sidebar shaded': {
        fg: {
            type:   'inherit',
            use:    'subsection in sidebar shaded'
        },
        bg: {
            type:   'inherit',
            use:    'subsection in sidebar shaded'
        }
    },

    'subsubsection number projected': {
        fg: {
            type:   'inherit',
            use:    'subsubitem projected'
        },
        bg: {
            type:   'inherit',
            use:    'subsubitem projected'
        }
    },

    'headline': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'footline': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'sidebar': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'sidebar left': {
        fg: {
            type:   'inherit',
            use:    'sidebar'
        },
        bg: {
            type:   'inherit',
            use:    'sidebar'
        }
    },

    'sidebar right': {
        fg: {
            type:   'inherit',
            use:    'sidebar'
        },
        bg: {
            type:   'inherit',
            use:    'sidebar'
        }
    },

    'logo': {
        fg: {
            type:   'inherit',
            use:    'palette secondary'
        },
        bg: {
            type:   'inherit',
            use:    'palette secondary'
        }
    },

    'frametitle': {
        fg: {
            type:   'inherit',
            use:    'titlelike'
        },
        bg: {
            type:   'inherit',
            use:    'titlelike'
        }
    },

    'framesubtitle': {
        fg: {
            type:   'inherit',
            use:    'frametitle'
        },
        bg: {
            type:   'inherit',
            use:    'frametitle'
        }
    },

    'frametitle right': {
        fg: {
            type:   'inherit',
            use:    'frametitle'
        },
        bg: {
            type:   'inherit',
            use:    'frametitle'
        }
    },

    'caption': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'caption name': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'inherit',
            use:    'structure'
        }
    },

    'button': {
        fg: {
            type:    'color',
            value:  'rgb(255,255,255)'
        },
        bg: {
            type:   'inherit attr mix',
            use:    'local structure',
            attr:   'fg',
            mix:    {
                strength:  50,
                type:   'attr',
                attr:   'bg',
                use:    bg_empty
            }
        }
    },

    'button border': {
        fg: {
            type:   'inherit',
            use:    'button'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'navigation symbols': {
        fg: {
            type:   'inherit mix',
            use:    'structure',
            mix:    {
                strength:  40,
                type:   'attr',
                attr:   'bg'
            }
        },
        bg: {
            type:   'empty',
            use:    'sidebar'
        }
    },

    'navigation symbols dimmed': {
        fg: {
            type:   'inherit mix',
            use:    'structure',
            mix:    {
                strength:  20,
                type:   'attr',
                attr:   'bg'
            }
        },
        bg: {
            type:   'empty',
            use:    'sidebar'
        }
    },

    'mini frame': {
        fg: {
            type:   'inherit',
            use:    'section in head/foot'
        },
        bg: {
            type:   'inherit',
            use:    'section in head/foot'
        }
    },

    'block body': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'block body alerted': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'block body example': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'block title': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'inherit',
            use:    'structure'
        }
    },

    'block title alerted': {
        fg: {
            type:   'inherit',
            use:    'alerted text'
        },
        bg: {
            type:   'inherit',
            use:    'alerted text'
        }
    },

    'block title example': {
        fg: {
            type:   'inherit',
            use:    'example text'
        },
        bg: {
            type:   'inherit',
            use:    'example text'
        }
    },

    'item': {
        fg: {
            type:   'inherit',
            use:    'local structure'
        },
        bg: {
            type:   'inherit',
            use:    'local structure'
        }
    },

    'subitem': {
        fg: {
            type:   'inherit',
            use:    'item'
        },
        bg: {
            type:   'inherit',
            use:    'item'
        }
    },

    'subsubitem': {
        fg: {
            type:   'inherit',
            use:    'subitem'
        },
        bg: {
            type:   'inherit',
            use:    'subitem'
        }
    },

    'item projected': {
        fg: {
            type:   'color',
            value:  'rgb(255,255,255)'
        },
        bg: {
            type:   'inherit attr',
            use:    'item',
            attr:   'fg'
        }
    },

    'subitem projected': {
        fg: {
            type:   'inherit',
            use:    'item projected'
        },
        bg: {
            type:   'inherit',
            use:    'item projected'
        }
    },

    'subsubitem projected': {
        fg: {
            type:   'inherit',
            use:    'subitem projected'
        },
        bg: {
            type:   'inherit',
            use:    'subitem projected'
        }
    },

    'enumerate item': {
        fg: {
            type:   'inherit',
            use:    'item'
        },
        bg: {
            type:   'inherit',
            use:    'item'
        }
    },

    'enumerate subitem': {
        fg: {
            type:   'inherit',
            use:    'subitem'
        },
        bg: {
            type:   'inherit',
            use:    'subitem'
        }
    },

    'enumerate subsubitem': {
        fg: {
            type:   'inherit',
            use:    'subsubitem'
        },
        bg: {
            type:   'inherit',
            use:    'subsubitem'
        }
    },

    'itemize item': {
        fg: {
            type:   'inherit',
            use:    'item'
        },
        bg: {
            type:   'inherit',
            use:    'item'
        }
    },

    'itemize subitem': {
        fg: {
            type:   'inherit',
            use:    'subitem'
        },
        bg: {
            type:   'inherit',
            use:    'subitem'
        }
    },

    'itemize subsubitem': {
        fg: {
            type:   'inherit',
            use:    'subsubitem'
        },
        bg: {
            type:   'inherit',
            use:    'subsubitem'
        }
    },

    'itemize/enumerate body': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'itemize/enumerate subbody': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'itemize/enumerate subsubbody': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'description item': {
        fg: {
            type:   'inherit',
            use:    'item'
        },
        bg: {
            type:   'inherit',
            use:    'item'
        }
    },

    'bibliography item': {
        fg: {
            type:   'inherit',
            use:    'item'
        },
        bg: {
            type:   'inherit',
            use:    'item'
        }
    },

    'bibliography entry author': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'bibliography entry title': {
        fg: {
            type:   'inherit',
            use:    'normal text'
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'bibliography entry location': {
        fg: {
            type:   'inherit mix',
            use:    'structure',
            mix: {
                strength:  65,
                type:    'attr',
                attr:   'bg'
            }
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'bibliography entry note': {
        fg: {
            type:   'inherit mix',
            use:    'structure',
            mix: {
                strength:  65,
                type:   'attr',
                attr:   'bg'
            }
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'separation line': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'upper separation line head': {
        fg: {
            type:   'inherit',
            use:    'separation line'
        },
        bg: {
            type:   'inherit',
            use:    'separation line'
        }
    },

    'middle separation line head': {
        fg: {
            type:   'inherit',
            use:    'separation line'
        },
        bg: {
            type:   'inherit',
            use:    'separation line'
        }
    },

    'lower separation line head': {
        fg: {
            type:   'inherit',
            use:    'separation line'
        },
        bg: {
            type:   'inherit',
            use:    'separation line'
        }
    },

    'upper separation line foot': {
        fg: {
            type:   'inherit',
            use:    'separation line'
        },
        bg: {
            type:   'inherit',
            use:    'separation line'
        }
    },

    'middle separation line foot': {
        fg: {
            type:   'inherit',
            use:    'separation line'
        },
        bg: {
            type:   'inherit',
            use:    'separation line'
        }
    },

    'lower separation line foot': {
        fg: {
            type:   'inherit',
            use:    'separation line'
        },
        bg: {
            type:   'inherit',
            use:    'separation line'
        }
    },

    'abstract': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'abstract title': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'inherit',
            use:    'structure'
        }
    },

    'verse': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'quotation': {
        fg: {
            type:   'empty',
            use:    fg_empty
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'quote': {
        fg: {
            type:   'inherit',
            use:    'quotation'
        },
        bg: {
            type:   'inherit',
            use:    'quotation'
        }
    },

    'page number in head/foot': {
        fg: {
            type:   'attr mix',
            attr:   'fg',
            use:    fg_empty,
            mix:    {
                strength:  50,
                type:   'attr',
                attr:   'bg',
                use:    bg_empty
            }
        },
        bg: {
            type:   'empty',
            use:    bg_empty
        }
    },

    'qed symbol': {
        fg: {
            type:   'inherit',
            use:    'structure'
        },
        bg: {
            type:   'inherit',
            use:    'structure'
        }
    },

    'note page': {
        fg: {
            type:   'color',
            value:  'rgb(0,0,0)'
        },
        bg: {
            type:   'color mix',
            value:  'rgb(255,255,255)',
            mix: {
                strength:   90,
                type:       'color',
                value:      'rgb(0,0,0)'
            }
        }
    },

    'note title': {
        fg: {
            type:   'color',
            value:  'rgb(0,0,0)'
        },
        bg: {
            type:   'color mix',
            value:  'rgb(255,255,255)',
            mix: {
                strength:   80,
                type:       'color',
                value:      'rgb(0,0,0)'
            }
        }
    },

    'note date': {
        fg: {
            type:   'inherit',
            use:    'note title'
        },
        bg: {
            type:   'inherit',
            use:    'note title'
        }
    }
};
