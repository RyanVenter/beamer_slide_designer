# -*- coding: utf-8 -*-

#===============================================================================
#=== IMPORTS ===================================================================
#===============================================================================
import os, glob
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
	render_template, flash, jsonify, send_from_directory

#from __future__ import print_function
import jinja2
import json
import subprocess
import shutil
# from collections import OrderedDict


#import locale
#import re
#import sys

#===============================================================================
#=== CREATE APPLICATION=========================================================
#===============================================================================
app = Flask(__name__)
app.config.from_object(__name__)

#===============================================================================
#=== CONFIGURE APP =============================================================
#===============================================================================
app.config.update(dict(
	DATABASE = os.path.join(app.root_path, 'bsd.db'),
	SECRET_KEY = '#!+27rV@s3CUr1T*93'
))

app.config.from_envvar('BSD_SETTTINGS', silent=True)

#===============================================================================
#=== DATABASE SETUP ============================================================
#===============================================================================
def connect_db():
	'''
	Connects to specific database.
	'''
	rv = sqlite3.connect(app.config['DATABASE'])
	rv.row_factory = sqlite3.Row
	return rv

def get_db():
	'''
	Opens a new database connection if there is none yet for the current
	application context.
	'''
	if not hasattr(g, 'db'):
		g.db = connect_db()
	return g.db

@app.teardown_appcontext
def close_db(error):
	'''
	Closes the database again at the end of the request.
	'''
	if hasattr(g, 'db'):
		g.db.close()

def init_db():
	db = get_db()
	c = db.cursor()

	with app.open_resource('schema.sql', mode='r') as f:
		c.executescript(f.read())

	with app.open_resource('init_elements.sql', mode='r') as f:
		c.executescript(f.read())

	db.commit()

@app.cli.command('initdb')
def initdb():
	'''
	Initialises the database.
	'''
	init_db()
	print('The database has been successfully initialised.')
#===============================================================================
#=== HELPER FUNCTIONS ==========================================================
#===============================================================================

def render_js(slide, elements):
	template_fname = 'templates/js/elements_template2.js'
	dest_fname = 'static/js/elements.js'

	env = jinja2.Environment(
		'%<', '>%',
		'<<', '>>',
		'[§', '§]',
		loader = jinja2.FileSystemLoader('.'),
		trim_blocks = True,
		lstrip_blocks = True
	)

	template = env.get_template(template_fname)

	vals = {}
	for elem in elements:
		n	 	= elem['name']
		vals[n]	= {}

		vals[n]['left']		= elem['left']
		vals[n]['top'] 		= elem['top']
		vals[n]['width'] 	= elem['width']
		vals[n]['height'] 	= elem['height']
		vals[n]['fg'] 		= elem['fg']
		vals[n]['bg'] 		= elem['bg']
		vals[n]['text'] 	= elem['text']
		vals[n]['fontSize'] = elem['fontSize']

	with open(dest_fname, 'w') as f:
		f.write(template.render(vals=vals))

def render_latex(elements):
	template_fname = 'templates/style_files/beamercolorthemedemo_template.sty'
	dest_fname = 'project/beamercolorthemedemo.sty'

	env = jinja2.Environment(
		'%<', '>%',
		'<<', '>>',
		'[§', '§]',
		loader = jinja2.FileSystemLoader('.'),
		trim_blocks = True,
		lstrip_blocks = True
	)

	template = env.get_template(template_fname)

	vals = {}
	for elem in elements:
		n	 	= elem['name']
		vals[n]	= {}

		vals[n]['fg'] 		= str(elem['fg']).replace("#","").upper()
		vals[n]['bg'] 		= str(elem['bg']).replace("#","").upper()
	with open(dest_fname, 'w') as f:
		f.write(template.render(vals=vals))


#===================================================================================================
#=== NEW JINJA FUNCTIONS ===========================================================================
#===================================================================================================

#===================================================================================================
#		Colortheme
#===================================================================================================
def color_value(value):
	start = value.find('(')+1
	end = value.find(')')
	return value[start:end]

def color_name(name,attr):
	return name.replace(' ', '') + '_' + attr

def new_color(name, attr, value, ext=''):
	return '\definecolor{'+color_name(name,attr)+ext+'}{RGB}{'+color_value(value)+'}'

def mix_extension():
	return '_mix'

def definecolors(color, key):
	fg = color[key]['fg']
	bg = color[key]['bg']

	colors = []

	# fg
	if 'color' in fg['type']:
		colors.append(new_color(key, 'fg',fg['value']))

	if 'mix' in fg['type']:
		if fg['mix']['type'] == 'color':
			colors.append(new_color(key, 'fg',fg['mix']['value'],mix_extension()))

	# bg
	if 'color' in bg['type']:
		colors.append(new_color(key, 'bg',bg['value']))

	if 'mix' in bg['type']:
		if bg['mix']['type'] == 'color':
			colors.append(new_color(key, 'bg',bg['mix']['value'],mix_extension()))


	return '\n'.join(colors)

def color_lineage(fg, bg):
	ancestors = []

	# color inheritance
	# fg
	if 'inherit' in fg['type']:
		ancestors.append(fg['use'])

	if 'mix' in fg['type']:
		if 'inherit' in fg['mix']['type']:
			ancestors.append(fg['mix']['use'])

	# bg
	if 'inherit' in bg['type']:
		ancestors.append(bg['use'])

	if 'mix' in bg['type']:
		if 'inherit' in bg['mix']['type']:
			ancestors.append(bg['mix']['use'])

	# result
	ancestors = list(set(ancestors))
	sz = len(ancestors)
	if sz == 0:
		return ''
	elif sz == 1:
		return 'use='+ancestors[0] + ', '
	else:
		return 'use={'+(','.join(ancestors))+'}, '


def set_color(c, name, attr, ext=''):
	if 'color' in c['type']:
		return color_name(name, attr) + ext

	elif 'inherit' in c['type']:
		ext = attr
		if 'attr' in c['type']:
			ext = c['attr']

		return c['use']+ '.' + ext

	elif 'attr' in c['type']:
		return c['attr']

	else:
		print('COLOR MODEL ERROR: Unrecognized color type - ', c['type'])


def color_settings(c, name, attr):
	result = ''
	# empty
	if c['type'] == 'empty':
		return ''
	# main color
	result += set_color(c, name, attr)

	# mixed color
	if 'mix' in c['type']:
		result += '!{0}!{1}'.format(c['mix']['strength'], set_color(c['mix'], name, attr, mix_extension()))

	return result

def setbeamercolor(color, key):
	fg = color[key]['fg']
	bg = color[key]['bg']

	# color parent
	if (fg['type'] == 'inherit') and (bg['type'] == 'inherit') and (fg['use'] == bg['use']):
		return 'parent=' + fg['use']

	# color lineage
	pre = color_lineage(fg, bg)

	# fg settings
	f = color_settings(fg, key, 'fg')
	fsz = len(f)
	sep = False
	if (fsz > 0):
		f = 'fg=' + f
		sep = True
	b = color_settings(bg, key, 'bg')
	bsz = len(b)
	if (bsz > 0):
		b = 'bg=' + b
		if (sep):
			f += ', '

	return (pre + f + b)

def render_color(keys, color, path, fname):
	template_fname = 'templates/style_files/beamercolorthemebsd_template.sty'
	dest_fname = '{0}beamercolortheme{1}.sty'.format(path, fname)

	env = jinja2.Environment(
		'?<', '>?',
		'<<', '>>',
		'[§', '§]',
		loader = jinja2.FileSystemLoader('.'),
		trim_blocks = True,
		lstrip_blocks = True
	)

	env.globals['setbeamercolor'] = setbeamercolor
	env.globals['definecolors'] = definecolors

	template = env.get_template(template_fname)
	with open(dest_fname, 'w') as f:
		f.write(template.render(keys=keys, color=color))

#===================================================================================================
#		Fonttheme
#===================================================================================================

def setbeamerfont(fonttheme, name):
	font = fonttheme[name]

	parent = ''
	parent_sep = ''

	size = ''
	size_sep = ''

	shape =  ''
	shape_sep = ''

	series = ''
	series_sep = ''

	family = ''

	if (font['parent'] != 'empty'):
		parent = 'parent='+ font['parent']

	if (font['size'] != 'empty'):
		if (parent):
			parent_sep = ', '
		size = 'size=\\' + font['size']

	if (font['shape'] != 'empty'):
		if (parent):
			parent_sep = ', '
		if (size):
			size_sep = ', '

		shape = 'shape=\\' + font['shape']

	if (font['series'] != 'empty'):
		if (parent):
			parent_sep = ', '
		if (size):
			size_sep = ', '
		if (shape):
			shape_sep= ', '

		series = 'series=\\' + font['series']

	if (font['family'] != 'empty'):
		if (parent):
			parent_sep = ', '
		if (size):
			size_sep = ', '
		if (shape):
			shape_sep= ', '
		if (series):
			series_sep= ', '

		family = 'family=\\' + font['family']

	return (parent + parent_sep + size + size_sep + shape + shape_sep + series + series_sep + family)

def render_font(font, path, fname):
	template_fname = 'templates/style_files/beamerfontthemebsd_template.sty'
	dest_fname = '{0}beamerfonttheme{1}.sty'.format(path, fname)

	env = jinja2.Environment(
		'?<', '>?',
		'<<', '>>',
		'[§', '§]',
		loader = jinja2.FileSystemLoader('.'),
		trim_blocks = True,
		lstrip_blocks = True
	)

	families = ['familydefault', 'sffamily', 'rmfamily', 'ttfamily', 'mathfamilydefault']

	env.globals['setbeamerfont'] = setbeamerfont

	template = env.get_template(template_fname)
	with open(dest_fname, 'w') as f:
		f.write(template.render(fonttheme=font, fname=fname, families=families))

#===================================================================================================
#		Innertheme
#===================================================================================================
def render_inner(inner, path, fname):
	template_fname = 'templates/style_files/beamerinnerthemebsd_template.sty'
	dest_fname = '{0}beamerinnertheme{1}.sty'.format(path, fname)

	env = jinja2.Environment(
		'?<', '>?',
		'<<', '>>',
		'[§', '§]',
		loader = jinja2.FileSystemLoader('.'),
		trim_blocks = True,
		lstrip_blocks = True
	)
	template = env.get_template(template_fname)
	with open(dest_fname, 'w') as f:
		f.write(template.render(innertheme=inner, fname=fname))

#===================================================================================================
#		Outertheme
#===================================================================================================
def render_outer(outer, path, fname):
	template_fname = 'templates/style_files/beamerouterthemebsd_template.sty'
	dest_fname = '{0}beameroutertheme{1}.sty'.format(path, fname)

	env = jinja2.Environment(
		'?<', '>?',
		'<<', '>>',
		'[§', '§]',
		loader = jinja2.FileSystemLoader('.'),
		trim_blocks = True,
		lstrip_blocks = True
	)
	template = env.get_template(template_fname)
	with open(dest_fname, 'w') as f:
		f.write(template.render(outertheme=outer, fname=fname))

#===================================================================================================
#		Theme
#===================================================================================================
def render_theme(config, path, fname):
	template_fname = 'templates/style_files/beamerthemebsd_template.sty'
	dest_fname = '{0}beamertheme{1}.sty'.format(path, fname)

	env = jinja2.Environment(
		'?<', '>?',
		'<<', '>>',
		'[§', '§]',
		loader = jinja2.FileSystemLoader('.'),
		trim_blocks = True,
		lstrip_blocks = True
	)
	template = env.get_template(template_fname)

	with open(dest_fname, 'w') as f:
		f.write(template.render(config=config, fname=fname))
#===================================================================================================
#		demo.tex
#===================================================================================================
def render_demo(config, inner, path, fname):
	template_fname = 'templates/tex/demo_template.tex'
	dest_fname = '{0}demo.tex'.format(path)

	env = jinja2.Environment(
		'?<', '>?',
		'<<', '>>',
		'[§', '§]',
		loader = jinja2.FileSystemLoader('.'),
		trim_blocks = True,
		lstrip_blocks = True
	)
	template = env.get_template(template_fname)

	with open(dest_fname, 'w') as f:
		f.write(template.render(config=config, innertheme=inner, fname=fname))

#===================================================================================================
#		Make theme package
#===================================================================================================
def package_theme(inner, path, fname):
	# subprocess.call('pdflatex '+ path+'demo.tex')
	try:
		shutil.copy('{0}beamertheme{1}.sty'.format(path, fname), '{0}bsd-theme-package/'.format(path))
		shutil.copy('{0}beamercolortheme{1}.sty'.format(path, fname), '{0}bsd-theme-package/'.format(path))
		shutil.copy('{0}beamerfonttheme{1}.sty'.format(path, fname), '{0}bsd-theme-package/'.format(path))
		shutil.copy('{0}beamerinnertheme{1}.sty'.format(path, fname), '{0}bsd-theme-package/'.format(path))
		shutil.copy('{0}beameroutertheme{1}.sty'.format(path, fname), '{0}bsd-theme-package/'.format(path))
		shutil.copy('{0}demo.tex'.format(path), '{0}bsd-theme-package/'.format(path))

		themegraphics = glob.glob(path+'bsd-theme-package/themegraphics/*')
		for f in themegraphics:
			os.remove(f)

		# Copy canvas graphics to theme package
		if inner['titlepagecanvasgraphic']:
			shutil.copy('{0}titlepagecanvasgraphic'.format(path), '{0}bsd-theme-package/themegraphics/'.format(path))
		if inner['toccanvasgraphic']:
			shutil.copy('{0}toccanvasgraphic'.format(path), '{0}bsd-theme-package/themegraphics/'.format(path))
		if inner['backgroundcanvasgraphic']:
			shutil.copy('{0}backgroundcanvasgraphic'.format(path), '{0}bsd-theme-package/themegraphics/'.format(path))

		# Copy graphics to theme package
		if inner['titlepagegraphic']['set']:
			shutil.copy('{0}titlepagegraphic'.format(path), '{0}bsd-theme-package/themegraphics/'.format(path))
		if inner['tocgraphic']['set']:
			shutil.copy('{0}tocgraphic'.format(path), '{0}bsd-theme-package/themegraphics/'.format(path))
		if inner['backgroundgraphic']['set']:
			shutil.copy('{0}backgroundgraphic'.format(path), '{0}bsd-theme-package/themegraphics/'.format(path))

		# eg. src and dest are the same file
	except shutil.Error as e:
		print('Error: %s' % e)
		# eg. source or destination doesn't exist
	except IOError as e:
		print('Error: %s' % e.strerror)

	# zip theme package directory
	existing = os.path.isdir('{0}bsd-theme-package'.format(path))
	print('{0}bsd-theme-package'.format(path), existing)

	shutil.make_archive('{0}bsd-theme-package'.format(path), 'zip', '{0}bsd-theme-package'.format(path))


#===============================================================================
#=== VIEW FUNCTIONS ============================================================
#===============================================================================
@app.route('/')
def home():
	return render_template('html/index.html')
#	db = get_db()
#	cur = db.execute('select * from elements where slide="'+ slide+'" or name="canvas"')
#	elements = cur.fetchall()

	#render_js(slide, elements)
#	render_html(slide)

#	cur = db.execute('select distinct slide from elements where slide<>"presentation"')
#	thumbnails = cur.fetchall()

	# session['active_slide'] = slide
	# session['active_elem'] = 'block'
	# session.modified = True


@app.route('/open_project', methods=['POST'])
def open_project():
	root = app.root_path + '/static/projects/'
	project_default = root+ 'default/default/'
	project_source = project_default

	path = root + request.json['path'] + '/'
	fname = request.json['fname']

	status = 'existing'
	existing = os.path.isdir(path)
	if existing:
		project_source = path
	else:
		print('building directory path..')
		os.makedirs(path + 'themegraphics/')
		os.makedirs(path + 'bsd-theme-package/themegraphics')

		status = 'new'

	# Load files from project source
	with open(project_source+'config.json') as f:
		config = json.load(f)
	with open(project_source+'colortheme.json') as f:
		color = json.load(f)
	with open(project_source+'fonttheme.json') as f:
		font = json.load(f)
	with open(project_source+'innertheme.json') as f:
		inner = json.load(f)
	with open(project_source+'outertheme.json') as f:
		outer = json.load(f)

	# If new project request save the default files in the new project directory tree path
	if not existing:
		with open(path+'config.json','w') as f:
			json.dump(config,f)
		with open(path+'colortheme.json','w') as f:
			json.dump(color,f)
		with open(path+'fonttheme.json','w') as f:
			json.dump(font,f)
		with open(path+'innertheme.json','w') as f:
			json.dump(inner,f)
		with open(path+'outertheme.json','w') as f:
			json.dump(outer,f)

	return jsonify({
		'status':	status,
		'config':	config,
		'color':	color,
		'font': 	font,
		'inner': 	inner,
		'outer': 	outer
		})

@app.route('/load_existing', methods=['POST'])
def load_existing():
	root 	= app.root_path + '/static/projects/'
	fname	= request.json['fname']
	path 	= root + fname.replace('_','/',1) + '/'
	target 	= request.json['target']

	if (fname == 'default_default'):
		return jsonify({ 'status': 'private'})
	else:
		existing = os.path.isdir(path)
		if not existing:
			return jsonify({ 'status': '404'})
		else:
			# Fetch json files from project directory tree
			if target == '':
				with open(path + 'config.json') as f:
					config = json.load(f)
				with open(path + 'colortheme.json') as f:
					color = json.load(f)
				with open(path + 'fonttheme.json') as f:
					font = json.load(f)
				with open(path + 'innertheme.json') as f:
					inner = json.load(f)
				with open(path + 'outertheme.json') as f:
					outer = json.load(f)
				# Remove themegraphics from existing project directory
				workingdir = root + request.json['path'] + '/'

				oldgraphics = glob.glob(workingdir +'themegraphics/*')
				for f in oldgraphics:
					os.remove(f)

				newgraphics = glob.glob(path +'themegraphics/*')
				for f in newgraphics:
					shutil.copy(f, workingdir + 'themegraphics/')

				return jsonify({
					'status':	'ok',
					'config':	config,
					'color':	color,
					'font': 	font,
					'inner': 	inner,
					'outer': 	outer
					});
			else:
				if target == 'inner':
					# Remove themegraphics from existing project directory
					workingdir = root + request.json['path'] + '/'

					oldgraphics = glob.glob(workingdir +'themegraphics/*')
					for f in oldgraphics:
						os.remove(f)

					newgraphics = glob.glob(path +'themegraphics/*')
					for f in newgraphics:
						shutil.copy(f, workingdir + 'themegraphics/')

				with open(path + target + 'theme.json') as f:
					settings = json.load(f)

				return jsonify({
					'status':	'ok',
					target:		settings
				})

@app.route('/save', methods=['POST'])
def save():
	root = app.root_path + '/static/projects/';
	project_default = root+'default/default/'

	keys = request.json['keys']
	config = request.json['config']
	color = request.json['color']
	font = request.json['font']
	inner = request.json['inner']
	outer = request.json['outer']

	path = root + request.json['id']['path'] + '/'
	if (request.json['id']['path'] == 'playground'):
		path = project_default

	fname = request.json['id']['fname']

	render_color(keys, color, path, fname)
	render_font(font, path, fname)
	render_inner(inner, path, fname)
	render_outer(outer, path, fname)
	render_theme(config, path, fname)

	render_demo(config, inner, path, fname)

	with open(path+'config.json','w') as f:
		json.dump(config,f)
	with open(path+'colortheme.json','w') as f:
		json.dump(color,f)
	with open(path+'fonttheme.json','w') as f:
		json.dump(font,f)
	with open(path+'innertheme.json','w') as f:
		json.dump(inner,f)
	with open(path+'outertheme.json','w') as f:
		json.dump(outer,f)

	package_theme(inner, path, fname);
	return jsonify({
		"status": "ok",
		})

@app.route('/package', methods=['POST'])
def package():
	path = request.json['path']
	fname = request.json['fname']
	package_theme(path, fname)

	return jsonify({"status": "ok"})


@app.route('/upload', methods=['POST'])
def upload():
	print('UPLOAD')



if __name__ == '__main__':
	app.run(debug=True)
