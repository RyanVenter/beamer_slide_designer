
/*
* =================================================================================================
*      Mini frames
* =================================================================================================
*/

/*
* =================================================================================================
*      Navigation symbols
* =================================================================================================
*/

/*
* =================================================================================================
*      Section and subsections in head/foot
* =================================================================================================
*/

/*
* =================================================================================================
*      Headline and footline
* =================================================================================================
*/

/*
* =================================================================================================
*      Background
* =================================================================================================
*/
var background_canvas = outerBox();
background_canvas.set({
    name: 'background canvas',
    attr: 'bg',

    width: paperwidth(),
    height: paperheight(),
    fill: getColor('background canvas', 'bg', colortheme['background canvas']['bg'])
});

makeImmutable(background_canvas);
outertemplates['background canvas'] = background_canvas;
/*
* =================================================================================================
*      Sidebar
* =================================================================================================
*/


/*
* =================================================================================================
*      Frame title
* =================================================================================================
*/

/*
* =================================================================================================
*      Frame title continuations
* =================================================================================================
*/

/*
* =================================================================================================
*      Notes
* =================================================================================================
*/
