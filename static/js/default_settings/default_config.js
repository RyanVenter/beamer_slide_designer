var dimensions = {
    "1610": {
        "ratiowidth":   16,
        "ratioheight":  10,
        "paperwidth":   160,    //measured in mm
        "paperheight":  100     //measured in mm
    },

    "169": {
        "ratiowidth":   16,
        "ratioheight":  9,
        "paperwidth":   160,    //measured in mm
        "paperheight":  90      //measured in mm
    },

    "149": {
        "ratiowidth":   14,
        "ratioheight":  9,
        "paperwidth":   140,    //measured in mm
        "paperheight":  90      //measured in mm
    },

    "54": {
        "ratiowidth":   5,
        "ratioheight":  4,
        "paperwidth":   125,    //measured in mm
        "paperheight":  100     //measured in mm
    },

    "43": {
        "ratiowidth":   4,
        "ratioheight":  3,
        "paperwidth":   128,    //measured in mm
        "paperheight":  96      //measured in mm
    },

    "32": {
        "ratiowidth":   3,
        "ratioheight":  2,
        "paperwidth":   135,    //measured in mm
        "paperheight":  90      //measured in mm
    },

    "141": {
        "ratiowidth":   1.4,
        "ratioheight":  1,
        "paperwidth":   148.5,  //measured in mm
        "paperheight":  105     //measured in mm
    }
};

default_config = {
    "aspectratio" :         "43",
    "basefontsize":         11,     //measured in pt
    "text margin left":     10,     //measured in mm
    "text margin right":    10,     //measured in mm
    "sidebar width left":   0,      //measured in mm
    "sidebar width right":  0,      //measured in mm
    "head":                 5,      //measured in mm
    "foot":                 5,      //measured in mm
    // "description width"
    // "description width of"
    "slidenum":             0,
};
