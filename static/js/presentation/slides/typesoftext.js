/*
* =================================================================================================
*      Types of text
* =================================================================================================
*/
typesoftextnum = addSlide('types of text');
slides[typesoftextnum]['verticalLayoutFunction'] = function () { setTypesOfTextInnerVerticalLayout(); };

var setTypesOfTextInnerVerticalLayout = function () {
    var entry           = innertheme['types of text'];
    var normalheading   = innertemplates['normal text heading'];
    var normaltext      = innertemplates['normal text'];
    var alertedheading  = innertemplates['alerted text heading'];
    var alertedtext     = innertemplates['alerted text'];

    var headingheight   = scalefactor*normalheading['height'];
    var normalheight    = scalefactor*normaltext['height'];
    var alertedheight   = scalefactor*alertedtext['height'];

    var gap             = scalefactor*15;
    var boxheights      = 2*headingheight + normalheight + alertedheight;

    var topmargin       = (scalefactor*textheight() - (boxheights + gap))/2;
    var currtop         = topmargin;

    normalheading.set({
        top:    currtop
    });
    currtop += headingheight;
    normaltext.set({
        top:    currtop
    });
    currtop += normalheight + gap;
    alertedheading.set({
        top:    currtop
    });
    currtop += headingheight;
    alertedtext.set({
        top:    currtop
    });

    slides[typesoftextnum].renderAll();
};

/* ---- Refresh slide fonts --------------------------------------------------------------------- */
var refreshTypesofTextFonts = function (name) {
    updateStructureFont('normal text heading');
    updateNormalTextFont();

    updateStructureFont('alerted text heading');
    updateTemplateFont(innertemplates['alerted text'],'types of text', typesoftextnum);

    setTypesOfTextInnerVerticalLayout();
    slides[typesoftextnum].renderAll();
    if (name != 'empty') {
        slides[typesoftextnum].setActiveObject(innertemplates[name]);
    }
    slides[typesoftextnum].renderAll();
};

/* ---- Refresh slide colors --------------------------------------------------------------------- */
var refreshTypesofTextColors = function () {
    var templates = ['normal text', 'alerted text'];
    for (var i in templates) {
        var name = templates[i]

        innertemplates[name + ' heading'].set({
            fill:   getColor('structure', 'fg', colortheme['structure']['fg'])
        });

        innerelements[name + ' text'].set({
            fill:   getColor(name, 'fg', colortheme[name]['fg'])
        });

        innerelements[name + ' box'].set({
            fill:   getColor('normal text', 'bg', colortheme['normal text']['bg'])
        });
    }
    slides[typesoftextnum].renderAll();
};

/* ---- Attach update function to slide -------------------------------------------------------- */
slides[typesoftextnum].refreshFontsFunction = function (name) { refreshTypesofTextFonts(name); };
slides[typesoftextnum].refreshColorsFunction = function () { refreshTypesofTextColors(); };
slides[typesoftextnum].refreshShadowsFunction = function () { };//shadow effects cannot be set for types of text

/*
* ------------------------------------------------------------------------------------------------
*      Normal text
* ------------------------------------------------------------------------------------------------
*/

//---- Normal text functions ---------------------------------------------------------------------

var updateNormalTextFont = function () {
    var template    = innertemplates['normal text'];
    var text        = innerelements['normal text text'];
    var box         = innerelements['normal text box'];
    var alignment   = innertheme['types of text']['normal text']['alignment'];


    text.set({
        fontSize:   getFontSize('structure'),
        fontFamily: getFontFamily('structure')
    });
    wrapText(text, typesoftextnum);
    box.set({
        left:   leftspace(),
        width:  textwidth()
    });

    setInnerBoxHeight('types of text', 'normal text');
    setInnerTextTop('types of text', 'normal text');
    initialiseInnerTextAlignment('types of text', 'normal text')


    slides[typesoftextnum].remove(normal_text);
    normal_text = immutableGroup('normal text', [text, box], typesoftextnum);
    addInnerTemplate(normal_text, typesoftextnum);
    setInnerTextAlignment('types of text', 'normal text', alignment, typesoftextnum);
};

var updateStructureFont = function (target) {
    var structure  = innertemplates[target];

    structure.set({
        fontSize:   getFontSize('structure'),
        fontFamily: getFontFamily('structure')
    });
    wrapText(structure, structure['slidenum'], textwidth());
};

//---- Normal text variables ---------------------------------------------------------------------

// var updateFontSize = function (target, template, slidenum, maxwidth=textwidth()) {
//     slides[slidenum].remove(target);
//
//     var name    = target['name'];
//     var text    = innerelements[name + ' text'];
//     var box     = innerelements[name + ' box'];
//
//     var alignment = innertheme[template][name]['alignment'];
//
//     text.set({
//         fontSize:   getFontSize(name)
//     });
//     wrapText(text, slidenum, maxwidth);
//     box.set({
//         left:   leftspace(),
//         width:  maxwidth
//     });
//     setInnerBoxHeight(template, name);
//     setInnerTextAlignment(template, name, alignment, slidenum);
//
//     target = immutableGroup(name, [text,box]);
//     addInnerTemplate(target, slidenum);
//     setTypesOfTextInnerVerticalLayout();
// };

// Normal text heading
var normal_text_heading = new templateHeading('normal text heading', 'Normal text:', typesoftextnum);
addInnerTemplate(normal_text_heading, typesoftextnum);

// Normal text box
var normal_text_box = innerBox('normal text box');
normal_text_box.set({
    fill:   getColor('normal text', 'bg', colortheme['normal text']['bg']),
});

// Normal text text
var normal_text_text = innerText('normal text text', innertheme['types of text']['normal text']['text']);
normal_text_text.set({
    fill:       getColor('normal text', 'fg', colortheme['normal text']['fg']),
    fontSize:	getFontSize('structure'),         // Normal text font  is currently ignored in beamer
    fontFamily: getFontFamily('structure')        // Normal text font is currently ignored in beamer

});
wrapText(normal_text_text, typesoftextnum, textwidth());
setInnerBoxHeight('types of text', 'normal text');
setInnerTextTop('types of text', 'normal text');
initialiseInnerTextAlignment('types of text', 'normal text')

// Normal text
var normal_text = immutableGroup('normal text', [normal_text_text, normal_text_box], typesoftextnum);
addInnerTemplate(normal_text, typesoftextnum);


/*
* ------------------------------------------------------------------------------------------------
*      Alerted text
* ------------------------------------------------------------------------------------------------
*/

// Alerted text heading
var alerted_text_heading = new templateHeading('alerted text heading', 'Alerted text:', typesoftextnum);
addInnerTemplate(alerted_text_heading, typesoftextnum);

// Alerted text box
var alerted_text_box = innerBox('alerted text box');
alerted_text_box.set({
    fill:   getColor('normal text', 'bg', colortheme['normal text']['bg']),
});

// Alerted text text
var alerted_text_text = innerText('alerted text text', innertheme['types of text']['alerted text']['text']);
alerted_text_text.set({
    fill:       getColor('alerted text', 'fg', colortheme['alerted text']['fg']),
    fontSize:	getFontSize('alerted text'),
    fontFamily: getFontFamily('alerted text')

});

wrapText(alerted_text_text, typesoftextnum, textwidth());
setInnerBoxHeight('types of text', 'alerted text');
setInnerTextTop('types of text', 'alerted text');
initialiseInnerTextAlignment('types of text', 'alerted text')

// Alerted text
var alerted_text = immutableGroup('alerted text', [alerted_text_text, alerted_text_box], typesoftextnum);
addInnerTemplate(alerted_text, typesoftextnum);

setTypesOfTextInnerVerticalLayout();
